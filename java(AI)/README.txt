*****************************
*Ryan Keating		    *
*Artificial Intelligence 335*
*Homework 4: Decision Trees *
*****************************

Description:
***********
Class Descriptions
=================

AttributeCriterion:
++++++++++++++++++
	A class that implements a selection criterion for use by a traditional decision tree learning algorithm.

ClassifierTree
+++++++++++++
	A class that implements a decision tree for classifying data.

CongressParser
+++++++++++++
	A parser class specifically created for the data file for congressional voting data

DataMember
++++++++++
	A class that implements a data member for a data set

DataParser
++++++++++
	An abstract class that provides parsing functionality not specific to a certain type of data.

DataSet
+++++++
	A class that holds a set of data members and information about the values they can take on.

Elitism
+++++++
	A class that implements elitism (preserving the best members from each generation) as a selection strategy.

FitnessFunction
+++++++++++++++
	A class the implements a fitness function to rate the performance of classifier trees.

FitnessProportionate
+++++++++++++++++++
	A class that implements the fitness proportionate beased selection strategy.

GainRatio
+++++++++
	A class that implements the gain ratio selection criterion.

GenerationLimit
+++++++++++++++
	A class that implements a termination condition for a limit on generations.

GenerationsWithoutImprovement
+++++++++++++++++++++++++++++
	A class that implements a limit on the number of generations to continue without significant improvement over the previous generation.

GeneticAlgorithm
++++++++++++++++
	A class that implements a genetic algorithm method for learning a decision tree.

InformationGain
+++++++++++++++
	A class that implements the information gain selection criterion.

LearningAlgorithm
+++++++++++++++++
	An abstract class that provides common functionality to both types of learning algorithms used to create decision trees.

MonkParser
++++++++++
	A parser class specifically created for the data files for monk data

MushroomParser
++++++++++++++
	A parser class specifically created for the data file for mushroom data

Node
+++++++++
	A class that implements decision tree nodes.

Precision
+++++++++
	A class the implements a precision fitness function.

PrecisionRecallCombination
+++++++++++++++++++++++++
	A class that implements a fitness function as a weighted average of precision and recall.

RankBased
++++++++++++
	Implements a selection strategy that ranks all population members and selects them for reproduction based on rank.

Recall
+++++++++++++
	A class the implements a recall fitness function.

ReplacementStrategy
+++++++++++++++++++
	A class that implements the replacemnt strategy used to generate a new population from a set of classifier trees.

SelectionStrategy
+++++++++++++++++
	A class that implements a strategy for selecting members of the population for reproduction.

TerminationCondition
+++++++++++++++++++
	An abstract that implements different types of termination conditions for a genetic algorithm.

TimeLimit
++++++++++++++
	A class that implements a termination condition with an approximate time limit. Note: The time limit is approximate in that it wont stop the genetic algorithm while the population is reproducing.

TotalReplacement
+++++++++++++++++++
	A class the implements the total replacement strategy.

TournamentBased
+++++++++++++++
	Implements a selection strategy that ranks a subset of population of size numParticipants chosen at random.

Traditional
+++++++++++
	A class that implements the traditional method for learning a decision tree.


================================================================================
Note: Package structure is would be very difficult to convey here. The javadocs
should do a much better job than I could.
================================================================================



Run instructions:
****************
1.While the root directory ryan.keating, the command “sh compile.sh” will run the program.
2.The program will prompt the user for all appropriate inputs

Note: The output files can also be used to check the proper input format (as I have included my input from the test runs).

Other instructions:
******************
To clean the bin directory of .class files:
==========================================
1.From the root directory ryan.keating, cd into src/
2.Enter the command “make clean”

Runtimes, etc.:
**************************************
	Traditional
	------------
		Regardless of the data set and selection criterion, a traditional learning algorithm runs almost immediately.
	Genetic
	--------—
		Population Size
		==============
			10
			\\\\
				GenerationLimit
				‘’’’’’’’’’’’’’’
					10:
						Runs in less than a second.
					30:
						
			20
			\\\\
				GenerationLimit
				‘’’’’’’’’’’’’’’
					10:

					30:



