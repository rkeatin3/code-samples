package decision_learning.classifier_tree;

import java.util.ArrayList;

/**
 * A class that implements decision tree nodes.
 */
public class Node{
  
  /**
   * The children of the node.
   */
  public ArrayList<Node> children = new ArrayList<Node>();
  
  /**
   * The attribute assigned to this node.
   */
  public int attribute;
  
  /**
   * The class that this node is assigned. Only for leaf nodes.
   */
  public int classType;
  
  /**
   * The no-argument constructor.
   */ 
  public Node(){}
  
  /**
   * The copy constructor.
   */
  public Node(Node n)
  {
    this.attribute = n.attribute;
    this.classType = n.classType;
  }
}