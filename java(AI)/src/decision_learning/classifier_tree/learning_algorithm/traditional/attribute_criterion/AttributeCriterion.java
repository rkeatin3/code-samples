package decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion;

import decision_learning.data.DataMember;

import java.lang.Math;
import java.util.ArrayList;

/**
 * A class that implements a selection criterion for use by a traditional decision tree learning algorithm.
 */
public abstract class AttributeCriterion{
  
  /**
   * Calculates the value of the selection criterion for the given attribute number of the given set of data members.
   *
   * @param members The data members on which to evaluate the criterion.
   *
   * @param attributeNum The attribute on which to evaluate the criterion.
   *
   * @return The value of the selection criterion.
   */
  public abstract double evaluate(ArrayList<DataMember> members, int attributeNum);
  
  /**
   * Calculates the information gain of splitting the given data members on the given attribute.
   *
   * @param members The data members to examine.
   *
   * @param attributeNum The attribute to consider.
   *
   * @return The information gained by splitting the data members on the attribute.
   */
  protected double informationGain(ArrayList<DataMember> members, int attributeNum)
  {
    double remainder = 0;
    double[] pn = getPN(members);
    double p = pn[0];
    double n = pn[1];
    for (int i = 0; i < members.get(0).attributeValues.length; i++) 
    {
      double[] pknk = getPkNk(members, attributeNum, i);
      double pk = pknk[0];
      double nk = pknk[1];

      //In the case where pk + nk = 0, the entropy will be undefined and cause an incorrect
      //vale for the remainder
      if ((pk + nk) != 0)
        remainder += ( (pk+nk) / (p+n) ) * entropy(pk / (nk + pk) );
    }
    return entropy(p / (n + p)) - remainder;
  }

  protected double informationValue(ArrayList<DataMember> members, int attributeNum)
  {
    double informationValue = 0;
    double[] pn = getPN(members);
    double p = pn[0];
    double n = pn[1];
    for (int i = 0; i < members.get(0).attributeValues.length; i++) 
    {
      double[] pknk = getPkNk(members, attributeNum, i);
      double pk = pknk[0];
      double nk = pknk[1];

      //Special case for 0 * entropy(NaN) 
      if ((pk + nk) != 0)
        informationValue -= ( (pk+nk) / (p+n) ) * log2( (pk+nk) / (p+n) );
    }
    return informationValue;
  }
  

  /**
   * Helper method which calculates the number of positive and negative data members in the given set.
   *
   * @param members The data members to examine.
   *
   * @return An array whose first value is the number of positive members and second is the number 
   * of negative members.
   */
  private double[] getPN(ArrayList<DataMember> members)
  {
    double[] pn = new double[]{0,0};
    for (int i = 0; i < members.size(); i++)
    {
      if (members.get(i).classType == 1)
      {
        pn[0] += 1;
      }
      else
      {
        pn[1] += 1;
      }
    }
    return pn;
  }
 
  /**
   * Helper method which calculates the number of positive and negative data members with the given 
   * value for the given attribute.
   *
   * @param members The data members to examine.
   *
   * @param attributeNum The attribute to consider.
   *
   * @param attributeValue The value to consider for the given attribute.
   *
   * @return An array whose first value is the number of positive members and second is the number 
   * of negative members.
   */
  private double[] getPkNk(ArrayList<DataMember> members, int attributeNum, int attributeValue)
  {
    double[] pknk = new double[]{0,0};
    for (int j = 0; j < members.size(); j++)
    {
      if (members.get(j).attributeValues[attributeNum] == attributeValue) 
      {
        if (members.get(j).classType == 1)
          pknk[0] += 1;
        else
          pknk[1] += 1;
      }
    }
    return pknk;
  }
  
  /**
   * Helper method which calculates the entropy of a Boolean random variable with probability q.
   *
   * @param q The probability of a true value for the Boolean random variable.
   *
   * @return The entropy of the variable.
   */
  private double entropy(double q)
  {
    if (q == 1 || q == 0)
    {
      return 0;
    }
    return -( q * log2(q) + (1-q) * log2(1 - q) );
  }
  
  /**
   * Helper method which calculates the log base 2 value of the input.
   *
   * @param The argument.
   *
   * @return Log base 2 of the argument.
   */
  private double log2(double num)
  {
    return Math.log(num)/Math.log(2);
  }
  
}