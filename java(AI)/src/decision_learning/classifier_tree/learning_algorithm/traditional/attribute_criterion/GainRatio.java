package decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion;

import decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion.AttributeCriterion;
import decision_learning.data.DataMember;

import java.util.ArrayList;

/**
 * A class that implements the gain ratio selection criterion.
 */
public class GainRatio extends AttributeCriterion {
  
  public double evaluate(ArrayList<DataMember> members, int attributeNum)
  {
    return informationGain(members, attributeNum)/informationValue(members, attributeNum);
  }
  
}