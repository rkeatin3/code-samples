package decision_learning.classifier_tree.learning_algorithm.traditional;

import decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion.AttributeCriterion;
import decision_learning.classifier_tree.learning_algorithm.LearningAlgorithm;
import decision_learning.data.DataMember;
import decision_learning.data.DataSet;
import decision_learning.classifier_tree.Node;


import java.util.ArrayList;
import java.util.Random;
import java.lang.Boolean;
import java.util.Arrays;

/**
 * A class that implements the traditional method for learning a decision tree.
 */
public class Traditional extends LearningAlgorithm{
  
  /**
   * The selection criterion used to determine which attribute best splits the data.
   */
  public AttributeCriterion crit;
  
  /**
   * The random number generator used to randomly break ties in the pluralityValue method.
   */
  Random rand = new Random();
  
  /**
   * The class constructor.
   *
   * @param crit The attribute selection criterion to be used.
   *
   * @param trainSet The data set to train on.
   */
  public Traditional(AttributeCriterion crit, DataSet trainSet)
  {
    this.crit = crit;
    this.trainSet = trainSet;
  }
  
  public Node learnTree()
  {
    ArrayList<Integer> attributesUsed = new ArrayList<Integer>();
    return decisionTreeLearn(trainSet.members, attributesUsed, trainSet.members);
  }
  
  /**
   * Implements the Decision-Tree_Learn algorithm from the course text.
   *
   * @param examples The data members with a specific value for the current node's attribute.
   *
   * @param attributesUsed An ArrayList containing the number values of attributes already assigned to nodes.
   *
   * @param parentExamples The data members passed to the previous call of the method.
   *
   * @return The subtree (or leaf node) returned.
   */
  private Node decisionTreeLearn(ArrayList<DataMember> examples, ArrayList<Integer> attributesUsed, ArrayList<DataMember> parentExamples)
  {
    ArrayList<Integer> newAttributesUsed = new ArrayList<Integer>();
    newAttributesUsed.addAll(attributesUsed);
    
    if (examples.size() == 0)
    {
      return pluralityValue(parentExamples);
    }
       
    Boolean allTypesSame = true;
    for (int i = 1; i < examples.size(); i++)
    {
      if (examples.get(i).classType != examples.get(0).classType)
      {
        allTypesSame = false;
        break;
      }
    }
    if (allTypesSame)
    {
      Node leaf = new Node();
      leaf.classType = examples.get(0).classType;
      return leaf;
    }
    
    if(newAttributesUsed.size() == trainSet.attributePossValues.length)
    {
      return pluralityValue(examples);
    }
    
    Node n = new Node();
    int a = findBestAttribute(examples, newAttributesUsed);
    n.attribute = a;
    newAttributesUsed.add(a);
    
    for (int i = 0; i < trainSet.attributePossValues[a]; i++)
    {

      ArrayList<DataMember> exs = new ArrayList<DataMember>();
      for (int j = 0; j < examples.size(); j++)
      {
        if (examples.get(j).attributeValues[a] == i) 
        {
          exs.add(examples.get(j));
        }
      }
      n.children.add(decisionTreeLearn(exs, newAttributesUsed, examples));
    } 
    return n; 
  }
  
  /**
   * Finds the best attribute to split the example data members by choosing the one that maximizes
   * the selection criterion.
   */
  private int findBestAttribute(ArrayList<DataMember> examples, ArrayList<Integer> attributesUsed)
  {
    double splitEntropy = Double.MIN_VALUE;
    int bestAttribute = 0;
    for (int i = 0; i < trainSet.attributePossValues.length; i++)
    {
      if (!attributesUsed.contains(i) && crit.evaluate(examples, i) > splitEntropy)
      {
        splitEntropy = crit.evaluate(examples, i);
        bestAttribute = i;
      }
    }
    return bestAttribute;
  }
  
  /**
   * Creates and returns a leaf node whose class type best represents the set of examples.
   *
   * @param examples The set of examples to examine.
   *
   * @return The node whose class type best represents the set of examples.
   */
  private Node pluralityValue(ArrayList<DataMember> examples)
  {
    int p = 0;
    Node leaf = new Node();
    for (int i = 0; i < examples.size(); i++)
    {
      if (examples.get(i).classType == 1)
      {
        p += 1;
      }
    }
    if (p > examples.size()/2)
    {
      leaf.classType = 1;
      return leaf;
    }
    else if (p < examples.size()/2)
    {
      leaf.classType = 0;
      return leaf;
    }
    else if (rand.nextDouble() < .5)
    {
       leaf.classType = 1;
       return leaf;
    }
    else
    {
       leaf.classType = 0;
       return leaf;
    }
  }
  
  
}