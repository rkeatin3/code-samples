package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.TerminationCondition;
import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataSet;

import java.util.ArrayList;
import java.lang.System;

/**
 * A class that implements a termination condition with an approximate time limit. 
 *
 * Note: The time limit is approximate in that it wont stop the genetic algorithm
 * while the population is reproducing.
 */
public class TimeLimit extends TerminationCondition{

  /**
   * The time limit in milliseconds.
   */
  private long limit;
  
  /**
   * The time at which the genetic algorithm began training.
   */
  private long startTime;
  
  /**
   * The class constructor.
   *
   * @param limit The time limit in seconds.
   */
  public TimeLimit(long limit)
  {
    this.limit = limit * 1000;
  }
  
  public boolean met(DataSet trainSet, ArrayList<ClassifierTree> population, int generations)
  {
    return ( (System.currentTimeMillis() - this.startTime) >= limit);
  }
  
  public void init(ArrayList<ClassifierTree> population)
  {
    this.startTime = System.currentTimeMillis();
  }

}