package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.TerminationCondition;
import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataSet;

import java.util.ArrayList;
/**
 * A class that implements a termination condition for a limit on generations.
 */
public class GenerationLimit extends TerminationCondition{

  private int limit;
  
  /**
   * The class constructor.
   *
   * @param limit The limit on the number of generations before the termination.
   */
  public GenerationLimit(int limit)
  {
    this.limit = limit;
  }
  
  public boolean met(DataSet trainSet, ArrayList<ClassifierTree> population, int generations)
  {
    return (generations >= limit);
  }
  
  public void init(ArrayList<ClassifierTree> population){}

}