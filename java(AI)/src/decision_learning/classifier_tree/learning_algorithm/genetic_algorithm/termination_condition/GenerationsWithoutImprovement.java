package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.TerminationCondition;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataSet;

import java.util.ArrayList;

/**
 * A class that implements a limit on the number of generations to continue without significant improvement over the previous generation.
 */
public class GenerationsWithoutImprovement extends TerminationCondition{

  /**
   * The number of generations to continue training without improvement.
   */
  private int limit;
  
  /**
   * A counter for how many generations have passed without improvement.
   */
  private int generationCount;
  
  /**
   * The average value of the fitness of the population.
   */
  private double avgFitness;
  
  /**
   * The fitness function used to determine whether the populations has improved or not.
   */
  private FitnessFunction fit;
  
  /**
   * The percentage/100 of fitness increase necessary to constitute significant improvement.
   */
  private double performanceThreshold = .05;
  
  /**
   * The class constructor.
   *
   * @param limit The limit on the number of generations to continue training without improvement.
   * 
   */
  public GenerationsWithoutImprovement(int limit,  FitnessFunction fit)
  {
    this.limit = limit;
    this.generationCount = 0;
    this.fit = fit;
  }
  
  public boolean met(DataSet trainSet, ArrayList<ClassifierTree> population, int generations)
  {
    checkFitness(population);
    return (generationCount >= limit);
  }
  
  public void init(ArrayList<ClassifierTree> population)
  {
    avgFitness = getAvgFitness(population);
  }

  /**
   * A helper method which checks for improvement over the last generation and updates
   * member varibles accordingly.
   *
   * @param population The population of the current generation.
   */
  private void checkFitness(ArrayList<ClassifierTree> population)
  {
    double newAvgFitness = getAvgFitness(population);
    if(newAvgFitness >= avgFitness * (1 + performanceThreshold)) 
    {
      avgFitness = newAvgFitness;
      generationCount = 0;
    }
    else 
    {
      avgFitness = newAvgFitness;
      generationCount += 1;
    }
  }
  
  private double getAvgFitness(ArrayList<ClassifierTree> population)
  {
    double[] fitnessValues = fit.getFitnessValues(population);
    double sum = 0;
    for (int i = 0; i < fitnessValues.length; i++)
    {
      sum += fitnessValues[i];
    }
    return (sum/fitnessValues.length);
  }
  
  
}