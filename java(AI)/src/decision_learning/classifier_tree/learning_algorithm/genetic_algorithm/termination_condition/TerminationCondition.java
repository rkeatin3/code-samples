package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition;

import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataSet;

import java.util.ArrayList;

/**
 * An abstract that implements different types of termination conditions for a genetic algorithm.
 */
public abstract class TerminationCondition{
  
  /**
   * Returns a boolean whose value indicates if the termination condition has been met.
   */
  public abstract boolean met(DataSet trainSet, ArrayList<ClassifierTree> population, int generations);
  
  /**
   * Initializes the termination condition. 
   * Note: For some termination conditions, this method will be empty.
   */
  public abstract void init(ArrayList<ClassifierTree> population);

}