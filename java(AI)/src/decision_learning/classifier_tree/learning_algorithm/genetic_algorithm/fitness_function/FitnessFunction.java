package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function;

import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataMember;
import decision_learning.data.DataSet;

import java.util.ArrayList;
import java.util.Random;


/**
 * A class the implements a fitness function to rate the performance of classifier trees.
 */
public abstract class FitnessFunction {
  
  /**
   * The data set to use in finding the performance metrics of classifier trees.
   */
  protected DataSet trainSet;
  
  /**
   * A boolean whose value represents whether or not the fitness function only uses a subset of 
   * the data set in calculating fitness.
   */
  protected boolean testOnSubset;
  
  /**
   * The size (in proportion to trainSet) of the subset to use.
   */
  protected double subsetProportion = .25;
  
  /*
   * The random number generator used to get a random subset of trainSet.
   */
  private Random rand = new Random();
  
  /**
   * Calculates the fitness value of the given classifier tree.
   *
   * @param tree The classifier tree to examine.
   *
   * @return The fitness value of the tree.
   */
  public abstract double evaluate(ClassifierTree tree);
  
  /**
   * Calculates the fitess values for all members of the population.
   *
   * @param population The population to examine.
   *
   * @return An array of the fitness values.
   */
  public double[] getFitnessValues(ArrayList<ClassifierTree> population)
  {
    double[] fitnessValues = new double[population.size()];
    for (int i = 0; i < fitnessValues.length; i++)
    {
      fitnessValues[i] = evaluate(population.get(i));
    }
    return fitnessValues;
  }
  
  protected double[] getMetrics(ClassifierTree tree)
  {
    if(testOnSubset == true)
    {
      ArrayList<DataMember> subsetMembers = new ArrayList<DataMember>();
      int numSubsetMembers =  (int) (trainSet.members.size()* subsetProportion);
      for (int i = 0; i < numSubsetMembers; i++)
      {
        //Chooses a data member from the train set and adds it to the subest 
        //as long as it has not already been added
        DataMember d = trainSet.members.get(rand.nextInt(trainSet.members.size()));
        while(subsetMembers.contains(d))
        {
          d = trainSet.members.get(rand.nextInt(trainSet.members.size()));
        }
        subsetMembers.add(d);
      }
      DataSet subset = new DataSet(subsetMembers);
      return tree.getTestMetrics(subset);
    }
    else
      return tree.getTestMetrics(trainSet);
  }
}