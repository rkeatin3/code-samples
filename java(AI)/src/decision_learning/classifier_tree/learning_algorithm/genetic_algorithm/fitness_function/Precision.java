package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataSet;

/**
 * A class the implements a precision fitness function.
 */
public class Precision extends FitnessFunction{
  
  /**
   * The class constructor.
   *
   * @param trainSet The dataSet to evaluate the fitness function on.
   */
  public Precision(DataSet trainSet)
  {
    this.trainSet = trainSet;
  }
  
  public double evaluate(ClassifierTree tree)
  {
    double[] metrics = getMetrics(tree);
    return metrics[1];
  }
}