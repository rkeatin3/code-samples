package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.data.DataSet;

/**
 * A class that implements a fitness function as a weighted average of precision and recall.
 */
public class PrecisionRecallCombination extends FitnessFunction{
  
  /**
   * The class constructor.
   *
   * @param precisionWeight The weight given to precision.
   *
   * @param trainSet The dataSet to evaluate the fitness function on.
   */
  public PrecisionRecallCombination(double precisionWeight, DataSet trainSet)
  {
    this.precisionWeight = precisionWeight;
    this.trainSet = trainSet;
  }
  
  /**
   * The weight given to precision.
   */
  private double precisionWeight;
  
  /**
   * The weight given to recall.
   */
  private double recallWeight = 1 - precisionWeight;

  public double evaluate(ClassifierTree tree)
  {
    double[] metrics = getMetrics(tree);
    return (precisionWeight * metrics[1] + recallWeight * metrics[1]);
  }
}