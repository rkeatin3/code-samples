package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.SelectionStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;

import java.util.ArrayList;

/**
 * A class that implements the fitness proportionate beased selection strategy.
 */
public class FitnessProportionate extends SelectionStrategy{
  
  /**
   * The class constructor.
   *
   * @param fit The fitness function used to evaluate population members.
   */
  public FitnessProportionate(FitnessFunction fit)
  {
    this.fit = fit;
  }
  
  protected double[] getWheel(ArrayList<ClassifierTree> members)
  {
    double[] fitnessValues = fit.getFitnessValues(members);
    int sumFitnessVals = 0;
    for (int i = 0; i < fitnessValues.length; i++)
    {
      sumFitnessVals  += fitnessValues[i];
    }
    double[] wheel = new double[members.size() + 1];
    wheel[0] = 0;
    
    for(int i = 0; i < fitnessValues.length; i++)
    {
      wheel[i + 1] = fitnessValues[i]/sumFitnessVals + wheel[i];
    }
    wheel = normalize(wheel);
    return wheel;
  }
  
  public ArrayList<ClassifierTree> select(ArrayList<ClassifierTree> population)
  {
    ArrayList<ClassifierTree> selectedMembers = new ArrayList<ClassifierTree>();
    for (int i = 0; i < population.size(); i++)
    {
      selectedMembers.add(population.get(spinWheel(getWheel(population))));
    }
    return selectedMembers;
  }
  
}