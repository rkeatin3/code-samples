package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.SelectionStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;

import java.util.ArrayList;

/**
 * Implements a selection strategy that ranks a subset of population 
 * of size numParticipants chosen at random.
 */
public class TournamentBased extends SelectionStrategy {
  
  /**
   * The number of participants in the tournament.
   */
  int numParticipants;
  
  /**
   * The class constructor.
   *
   * @param fit The fitness function used to evaluate population members.
   */
  public TournamentBased(FitnessFunction fit, int numParticipants)
  {
    this.fit = fit;
    this.numParticipants = numParticipants;
  }
  
  protected double[] getWheel(ArrayList<ClassifierTree> population)
  {
    //Chooses a participant from the population at random (making sure it's not
    //already in the set of participants.
    ArrayList<ClassifierTree> participants = new ArrayList<ClassifierTree>();
    for (int i = 0; i < numParticipants; i++)
    {
      ClassifierTree participant = population.get(rand.nextInt(population.size()));
      while(participants.contains(participant))
      {
        participant = population.get(rand.nextInt(population.size()));
      }
      participants.add(participant);
    }
    return getRankedWheel(participants.size());
  }

  public ArrayList<ClassifierTree> select(ArrayList<ClassifierTree> participants)
  {
    return selectRanked(participants);
  }

}