package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.SelectionStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;

import java.util.ArrayList;

/**
 * Implements a selection strategy that ranks all population members
 * and selects them for reproduction based on rank.
 */
public class RankBased extends SelectionStrategy {
  
  /**
   * The class constructor.
   *
   * @param fit The fitness function used to evaluate population members.
   */
  public RankBased(FitnessFunction fit)
  {
    this.fit = fit;
  }
  
  protected double[] getWheel(ArrayList<ClassifierTree> population)
  {
    return getRankedWheel(population.size());
  }

  public ArrayList<ClassifierTree> select(ArrayList<ClassifierTree> population)
  {
    return selectRanked(population);
  }

}