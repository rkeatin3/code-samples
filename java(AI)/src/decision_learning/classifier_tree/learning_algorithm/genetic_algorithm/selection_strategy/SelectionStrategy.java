package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;

import java.util.ArrayList;
import java.util.Random;

/**
 * A class that implements a strategy for selecting members of the population for reproduction.
 */
public abstract class SelectionStrategy{
  
  /**
   * The fitness function used to evaluate population members.
   */
  public FitnessFunction fit;
  
  /*
   * The random number generator used to randomly generate values on the range [0,1).
   */
  Random rand = new Random();
  
  /**
   * Creates a set of referencs to classifier trees (of same size of the original population) by
   * selecting its members from the original population based on fitness.
   */
  public abstract ArrayList<ClassifierTree> select(ArrayList<ClassifierTree> population);
  
  /**
   * Gets the "roullette wheel" for the selection probabilities.
   * This is an array of values from 0 to 1, splt into N sections,
   * where N is the number of members chosen to select from. The size
   * of each section corresponds to the proportional probability of 
   * choosing the member assigned to that section.
   * 
   * @return  The roullette wheel.
   */
  protected abstract double[] getWheel(ArrayList<ClassifierTree> members);
  
  /**
   * Gets the roullette wheel based on ranking of N members.
   */
  protected double[] getRankedWheel(int N)
  {
    double[] wheel = new double[N + 1];
    wheel[0] = 0;
    for (int i = 0; i < N; i++)
    {
      wheel[i + 1] = wheel[i] + (N - i);
    }
    wheel = normalize(wheel);
    return wheel;
  }
  
  /**
   * Normalizes the given wheel so that the values range from 0 to 1
   */
  protected double[] normalize(double[] wheel)
  {
    for (int i = 1; i < wheel.length; i++)
    {
      wheel[i] = wheel[i]/wheel[wheel.length - 1];
    }
    return wheel;
  }
  
  /**
   * Spins the roullette wheel.
   * This means choosing a value from [0,1) and returning the index
   * corresponding to the section of the wheel it is within
   */
  protected int spinWheel(double[] wheel)
  {
    double val = rand.nextDouble();
    
    int i = 0;
    while(val > wheel[i + 1])
    {
      i++;
    }
    return i;
  }
  
  
  protected ArrayList<ClassifierTree> selectRanked(ArrayList<ClassifierTree> members)
  {
    double[] fitnessValues= fit.getFitnessValues(members);
    ArrayList<Double> rankedIndices = new ArrayList<Double>();
    for (int i = 0; i < members.size(); i++)
    {
      int indexHighestFit = indexHighestFit(members, rankedIndices);
      rankedIndices.add((double) indexHighestFit);
    }
    ArrayList<ClassifierTree> selectedMembers = new ArrayList<ClassifierTree>();
    double wheel[] = getWheel(members);
    
    for (int i = 0; i < members.size(); i++)
    {
      rankedIndices.get(spinWheel(wheel));
      selectedMembers.add(members.get((int)rankedIndices.get(spinWheel(wheel)).doubleValue()));
    }
    return selectedMembers;
  }
  
  /**
   * A helper method that returns the index of the member with the highest fit 
   * (as long as its index has not yet been added to rankedIndices).
   *
   * @param members The members to rank by fitness.
   *
   * @param rankedIndices An array list of the indices of all the members in order of rank.
   *
   * @preturn The index of the member with the highest fit.
   */
  private int indexHighestFit(ArrayList<ClassifierTree> members, ArrayList<Double> rankedIndices)
  {
    int indexHighestFit = 0;
    double highestFit = -Double.MAX_VALUE;
    
    
    double[] fitnessValues = fit.getFitnessValues(members);
    for(int i = 0; i < fitnessValues.length; i++)
    {
      if(!rankedIndices.contains(i) && fitnessValues[i] > highestFit)
      {
        indexHighestFit = i;
        highestFit = fitnessValues[i];
      }
    }
  return indexHighestFit;
  }
}
