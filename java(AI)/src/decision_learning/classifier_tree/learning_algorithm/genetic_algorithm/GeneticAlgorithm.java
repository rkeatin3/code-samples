package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.TerminationCondition;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy.ReplacementStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.SelectionStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.learning_algorithm.LearningAlgorithm;

import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.classifier_tree.Node;

import decision_learning.data.DataSet;
import java.util.ArrayList;
import java.util.Random;

/**
 * A class that implements a genetic algorithm method for learning a decision tree.
 */
public class GeneticAlgorithm extends LearningAlgorithm {
  
  /**
   * The number of decision trees in the population.
   */
  private int populationSize;
  
  /**
   * The condition which determines when to stop training the population.
   */
  private TerminationCondition term;
  
  /**
   * The strategy used to replace the population by mating and mutating its members.
   */
  private ReplacementStrategy rep;
  
  /**
   * The strategy used to select the best population members.
   */
  private SelectionStrategy sel;
  
  /**
   * The population of decision trees.
   */
  private ArrayList<ClassifierTree> population;
  
  /**
   * The number of generations that the genetic algorithm has trained for.
   */
  private int generations;
  
  /**
   * The data set that the algorithm is being trained on.
   */
  private DataSet trainSet;
  
  /*
   * The random number generator used to create random decision trees for the first generation.
   */
  Random rand = new Random();
  
  public GeneticAlgorithm(int populationSize, TerminationCondition term, SelectionStrategy sel, 
    ReplacementStrategy rep, DataSet trainSet)
  {
    this.rep = rep;
    this.trainSet = trainSet;
    this.populationSize = populationSize;
    this.term = term;
    this.sel = sel;
    this.generations = 1;
    this.population = new ArrayList<ClassifierTree>();
    for (int i = 0; i < populationSize; i++)
    {
      this.population.add(getRandomClassifierTree());
    }
  }
  
  public Node learnTree()
  {
    term.init(population);
    while(!term.met(trainSet, population, generations))
    {
      rep.reproduce(sel.select(population));
      generations += 1;
    }
    return getMostFit(population);
  }
  
  /**
   * A helper method that gets a random classifier tree.
   *
   * @return A random classifier tree.
   */
  private ClassifierTree getRandomClassifierTree()
  {
    ArrayList<Double> attributesToUse = new ArrayList<Double>();
    for (int i = 0; i < trainSet.attributePossValues.length; i++)
    {
      attributesToUse.add((double) i);
    }
    return new ClassifierTree(genRandomSubtree(attributesToUse));
  }

  /**
   * A helper method that creates a decision tree by randomly assigning attributes and class types.
   *
   * @param attributesToUse An array list of attribute values to assign.
   * 
   * @return The decision tree structure.
   */
  private Node genRandomSubtree(ArrayList<Double> attributesToUse)
  {
    ArrayList<Double> attributesNotUsed = new ArrayList<Double>();
    attributesNotUsed.addAll(attributesToUse);
    double leafProb = rand.nextDouble();
    double thresh = .2;
    if (attributesNotUsed.size() == 0 || leafProb < thresh)
    {
      Node leaf = new Node();
      if (rand.nextDouble() < .5)
        leaf.classType = 1;
      else
        leaf.classType = 0;
      return leaf;
    }
    
    Node n = new Node();
    int a = (int) attributesNotUsed.get(rand.nextInt(attributesNotUsed.size())).doubleValue(); 
    n.attribute = a;
    attributesNotUsed.remove((double) a);
    
    for (int i = 0; i < trainSet.attributePossValues[a]; i++)
    {
      n.children.add(genRandomSubtree(attributesNotUsed));
    }
    return n;
  }
  
  /**
   * A helper function that returns the root of the highest performing classifer tree in the population.
   *
   * @param The population to examine.
   *
   * @return The root of the highest performing classifer tree.
   */ 
  private Node getMostFit(ArrayList<ClassifierTree> population)
  {
    double highestFitness = Double.MIN_VALUE;
    int indexHighestFit = 0;
    for (int i = 0; i < population.size(); i++)
    {
      if(sel.fit.evaluate(population.get(i)) > highestFitness)
      {
        highestFitness = sel.fit.evaluate(population.get(i));
        indexHighestFit = i;
      }
    }
    return population.get(indexHighestFit).root;
  }
}