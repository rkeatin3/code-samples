package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy.ReplacementStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.ClassifierTree;


import java.util.ArrayList;

/**
 * A class that implements elitism (preserving the best members from each generation) as a selection strategy.
 */
public class Elitism extends ReplacementStrategy{
  
  /**
   * The number of members to preserve from the previous population.
   *
   * Note: It is theoretically possible that selectedMembers will not contain enough unique
   * members to satisty this number.
   */
  private int numEliteMembers;
  
  /**
   * The fitness function used to choose which selecteMembers should be preserved in the next population.
   */
  FitnessFunction fit;
  
  /**
   * The class constructor.
   *
   * @param fit The fitness function used to evaluate population members.
   *
   * @param attributePossValues An array that indicates the number of possible values for each attribute the data members have.
   */
  public Elitism(FitnessFunction fit, int numEliteMembers, int[] attributePossValues)
  {
    this.fit = fit;
    this.numEliteMembers = numEliteMembers;
    this.attributePossValues = attributePossValues;
  }
  
  public ArrayList<ClassifierTree> reproduce(ArrayList<ClassifierTree> selectedMembers)
  {
    ArrayList<ClassifierTree> eliteMembers = getEliteMembers(selectedMembers);
    ArrayList<ClassifierTree> offspring = new ArrayList<ClassifierTree>();
    for (int i = 0; i < eliteMembers.size(); i++)
    {
      offspring.add(new ClassifierTree(eliteMembers.get(i)));
    }
    for (int i = 0; i < selectedMembers.size(); i++)
    {
      ClassifierTree t = new ClassifierTree(selectedMembers.get(i));
      if(rand.nextDouble() <= crossoverProb)
      {
        /*
        Randomly choose a second classifier tree for crossover, ensuring it isn't the same tree
        Note: == is used because selectedMembers is just a set of references to the classifier trees
        selected from the previous population.
        */
        int parent2 = rand.nextInt(selectedMembers.size());
        while (selectedMembers.get(i) == selectedMembers.get(parent2))
        {
          parent2 = rand.nextInt(selectedMembers.size());
        }
        offspring.add(crossover(t, selectedMembers.get(parent2)));
      }
      else 
      {
        offspring.add(t);
      }
    }
    System.out.println(offspring.size());
    return mutatePopulation(offspring);
  }
  
  /**
   *
   */
  private ArrayList<ClassifierTree> getEliteMembers(ArrayList<ClassifierTree> selectedMembers)
  {
    //Create the set of elite members and initially adds the first numEliteMembers of selectedMembers
    ArrayList<ClassifierTree> eliteMembers = new ArrayList<ClassifierTree>();
    ArrayList<Double> eliteFitnessVals = new ArrayList<Double>();
    for (int i = 0; i < numEliteMembers; i++)
    {
      selectedMembers.get(i); 
      eliteMembers.add(selectedMembers.get(i));
      eliteFitnessVals.add(fit.evaluate(selectedMembers.get(i)));
    }
    
    selectedMembers.removeAll(eliteMembers);
    
    for(int i = 0; i < selectedMembers.size(); i++)
    {
      double fitVal = fit.evaluate(selectedMembers.get(i));
      if(betterThanWorst(fitVal, eliteMembers, eliteFitnessVals, selectedMembers))
      {
        eliteFitnessVals.add(fitVal);
        eliteMembers.add(selectedMembers.get(i));
        selectedMembers.remove(i);
      }
    }
    return eliteMembers;   
  }
  
  /**
   * A helper method that tests if fitVal qualifies its classifier tree to replace one of the elite members.
   * If so, it removes the elite member to be replace and adds it to selectedMembers.
   *
   * @param fitVal The fitness value to consider.
   *
   * @param eliteMembers The set of elite members.
   *
   * @param eliteFitnessVals The set of fitness values that corresponds to the elite members.
   *
   * @param selectedMembers The set of classifier trees chosen for reproduction by the selection strategy.
   *
   * @return Whether or not fitVal qualifies its classifier tree to replace one of the elite members.
   */
  private boolean betterThanWorst(double fitVal, ArrayList<ClassifierTree> eliteMembers, ArrayList<Double> eliteFitnessVals, 
    ArrayList<ClassifierTree> selectedMembers)
  {
    double lowestFitness = Double.MAX_VALUE;
    double location = 0;
    for(int i = 0; i < eliteMembers.size(); i++)
    {
      if(fitVal > eliteFitnessVals.get(i))
      {
        selectedMembers.add(eliteMembers.get(i));
        eliteMembers.remove(i);
        eliteFitnessVals.remove(i);
        return true;
      }
    }
    return false;
  }
  
  
 
  
}