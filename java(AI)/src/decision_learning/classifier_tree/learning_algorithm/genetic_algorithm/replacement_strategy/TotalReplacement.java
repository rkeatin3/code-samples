package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy.ReplacementStrategy;
import decision_learning.classifier_tree.ClassifierTree;

import java.util.ArrayList;

/**
 * A class the implements the total replacement strategy.
 */
public class TotalReplacement extends ReplacementStrategy{
  
  /**
   * The class constructor.
   *
   * @param attributePossValues An array that indicates the number of possible values for each attribute the data members have.
   */
  public TotalReplacement(int[] attributePossValues)
  {
    this.attributePossValues = attributePossValues;
  }
  
  public ArrayList<ClassifierTree> reproduce(ArrayList<ClassifierTree> selectedMembers)
  {
    ArrayList<ClassifierTree> offspring = new ArrayList<ClassifierTree>();
    for(int i = 0; i < selectedMembers.size(); i++)
    {
      ClassifierTree t = new ClassifierTree(selectedMembers.get(i));
      if(rand.nextDouble() <= crossoverProb)
      {
        /*
        Randomly choose a second classifier tree for crossover, ensuring it isn't the same tree
        Note: == is used because selectedMembers is just a set of references to the classifier trees
        selected from the previous populaiton.
        */
        int parent2 = rand.nextInt(selectedMembers.size());
        while (selectedMembers.get(i) == selectedMembers.get(parent2))
        {
          parent2 = rand.nextInt(selectedMembers.size());
        }
        offspring.add(crossover(t, selectedMembers.get(parent2)));
      }
      else 
      {
        offspring.add(t);
      }
    }
    return mutatePopulation(offspring);
  }
  
}