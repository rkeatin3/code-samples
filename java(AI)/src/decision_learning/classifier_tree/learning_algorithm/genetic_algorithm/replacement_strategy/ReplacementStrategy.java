package decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy;

import decision_learning.classifier_tree.ClassifierTree;
import decision_learning.classifier_tree.Node;

import java.util.ArrayList;
import java.util.Random;

/**
 * A class that implements the replacemnt strategy used to generate a new population from a set of classifier trees.
 */
public abstract class ReplacementStrategy {
  
  /**
   * The probability of a mutation during reproduction.
   */ 
  protected double mutationProb = .7;
  
  /**
   * The probability of a crossover during reproduction.
   */
  protected double crossoverProb = .8;
  
  /**
   * An array that indicates the number of possible values for each attribute the data members have.
   */
  protected int[] attributePossValues;
  
  /**
   * An array list that contains the attribute number of all of the parents of the subtree 
   * returned by the chooseRandomSubtree method.
   */
  private ArrayList<Double> subtreeParentAttributes = new ArrayList<Double>();
  
  /*
   * The random number generator used to randomly create subtrees, choose subtrees, and assign class types.
   */
  Random rand = new Random();

  /**
   * Creates a new population by reproduction (with possible crossover and mutation).
   *
   * @param selectedMembers The set of classifier trees chosen for reproduction by the selection strategy.
   *
   * @return The new population.
   */
  public abstract ArrayList<ClassifierTree> reproduce(ArrayList<ClassifierTree> selectedMembers);
  
  protected ClassifierTree crossover(ClassifierTree parent1, ClassifierTree parent2)
  {
    ClassifierTree child = new ClassifierTree(parent1);
    Node childSubtree = chooseRandomSubtree(child);
    childSubtree = chooseRandomSubtree(parent2);
    return child;
  }
  
  /**
   * For each member of the population, chooses with probability mutationProb to mutate it or leave it as is.
   *
   * @param population The population to examine.
   *
   * @return The population with some possibly mutated members.
   */
  public ArrayList<ClassifierTree> mutatePopulation(ArrayList<ClassifierTree> population)
  {
    for(int i = 0; i < population.size(); i++)
    {
      if(rand.nextDouble() <= mutationProb)
      {
        mutate(population.get(i));
      }
    }
    return population;
  }
  
  
  /**
   * A method that randomly chooses between the three viable mutations on a decision tree:
   * 1. Replacing a subtree with a new, randomized subtree (with the same attributes as the original).
   * 2. Replacing a subtree with a leaf node of randomized class type.
   * 3. Replaces a leaf node with depth-1 subtree in which the class types of its leaves are randomized.
   *
   * @param tree The tree to mutate.
   *
   * @return The mutated tree.
   */
  private ClassifierTree mutate(ClassifierTree tree)
  {
    double type = rand.nextDouble();
    
    if(type <= .33)
    {
      return changeSubtree(tree);
    }
    else if (type <= .67)
    {
      return deleteSubtree(tree);
    }
    else
    {
      return addNode(tree);
    }
  }
 
  /**
   * Mutates the given tree in the following way: Chooses a non-leaf node (i.e. subtree) at random,
   * then replaces it with a new randomized subtree containing the same attributes as the original.
   *
   * @param The tree to mutate.
   *
   * @return The mutated tree.
   */
  private ClassifierTree changeSubtree(ClassifierTree tree)
  {
    Node n = chooseRandomSubtree(tree);
    ArrayList<Double> attributesToUse = new ArrayList<Double>();
    for (int i = 0; i < attributePossValues.length; i++) 
    {
      attributesToUse.add((double) i);
    }
    attributesToUse.removeAll(subtreeParentAttributes);
    subtreeParentAttributes.clear();
    n = genRandomSubtree(attributesToUse);
    return tree;
  }
 
  /**
   * Mutates the given tree in the following way: Chooses a non-leaf node (i.e. subtree) at random,
   * then replaces it with a leaf of randomized class type.
   *
   * @param The tree to mutate.
   *
   * @return The mutated tree.
   */
  private ClassifierTree deleteSubtree(ClassifierTree tree)
  {
    int depth = rand.nextInt(attributePossValues.length);
    Node n = tree.root;
    for (int i = 0; i < depth; i++)
    {
      if(n.children.size() == 0)
        break;
      n = n.children.get(rand.nextInt(n.children.size()));
    }
    n.children.clear();
    n.classType = rand.nextInt(1);
    return tree;
  }
  
  /**
   * Mutates the given tree in the following way: Chooses a leaf node at random,
   * then replaces it with depth-1 subtree. The leaf nodes of the subtree have
   * randomized value.
   *
   * @param The tree to mutate.
   *
   * @return The mutated tree.
   */
  private ClassifierTree addNode(ClassifierTree tree)
  {
    Node n = tree.root;
    while (n.children.size() > 0)
    {
      n = n.children.get(rand.nextInt(n.children.size()));
    }
    n.classType = -1;
    n.attribute = rand.nextInt(attributePossValues.length);
    for (int i = 0; i < attributePossValues[n.attribute]; i++)
    {
      Node leaf = new Node();
      leaf.classType = rand.nextInt(1);
      n.children.add(leaf);
    }
    return tree;
  }
  
  /**
   * A helper method which chooses a random, non-leaf node (i.e. subtree) from the given tree.
   *
   * @param The tree to examine.
   *
   * @return The random subtree.
   */
  private Node chooseRandomSubtree(ClassifierTree tree)
  {
    int depth = rand.nextInt(attributePossValues.length);
    Node n = tree.root;
    for (int i = 1; i < depth; i++)
    {
      if (n.children.size() == 0)
      {
        subtreeParentAttributes.clear();
        return chooseRandomSubtree(tree);
      }
      subtreeParentAttributes.add((double) n.attribute);
      n = n.children.get(rand.nextInt(n.children.size()));
    }
    return n;
  }
  
  /**
   * A helper method that creates a decision tree by randomly assigning attributes and class types.
   *
   * @param attributesToUse An array list of attribute values to assign.
   * 
   * @return The decision tree structure.
   */
  private Node genRandomSubtree(ArrayList<Double> attributesToUse)
  {
    ArrayList<Double> attributesNotUsed = new ArrayList<Double>();
    attributesNotUsed.addAll(attributesToUse);
    if (attributesNotUsed.size() == 0)
    {
      Node leaf = new Node();
      if (rand.nextDouble() < .5)
        leaf.classType = 1;
      else
        leaf.classType = 0;
      return leaf;
    }
    
    Node n = new Node();
    int a = (int) attributesNotUsed.get(rand.nextInt(attributesNotUsed.size())).doubleValue();
    n.attribute = a;
    attributesNotUsed.remove((double) a);
    
    for (int i = 0; i < attributePossValues[a]; i++)
    {
      n.children.add(genRandomSubtree(attributesNotUsed));
    }
    return n;
  }
  
}