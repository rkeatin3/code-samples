package decision_learning.classifier_tree.learning_algorithm;

import decision_learning.classifier_tree.Node;
import decision_learning.data.DataSet;

import java.util.ArrayList;

/**
 * An abstract class that provides common functionality to both types of learning algorithms used to create decision trees.
 */
public abstract class LearningAlgorithm {
  
  /**
   * The data set used for learning.
   */
  public DataSet trainSet;
  
  /**
   * Learns a decision tree.
   *
   * @return The decision tree.
   */
  public abstract Node learnTree();

}