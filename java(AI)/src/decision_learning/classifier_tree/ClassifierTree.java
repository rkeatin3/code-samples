package decision_learning.classifier_tree;

import decision_learning.classifier_tree.Node;
import decision_learning.data.DataMember;
import decision_learning.data.DataSet;


import java.util.ArrayList;

/**
 * A class that implements a decision tree for classifying data.
 *
 * @author Ryan Keating
 */
public class ClassifierTree{
  
  /**
   * The root node of the decision tree.
   */
  public Node root;
  

  /**
   * The class constructor.
   *
   * @param root The structure of the decision tree (learned elsewhere).
   */
  public ClassifierTree(Node root)
  {
    this.root = root;
  }
  
  /**
   * Tests the decision tree by classifying the data and calculating performance metrics.
   *
   * @param data The data set to test on.
   *
   * @return An array containing the accuracy, precision, and recall (in that order) as percentages.
   */
  public double[] getTestMetrics(DataSet data)
  {
    double tp = 0;
    double fp = 0;
    double tn = 0;
    double fn = 0;
    
    for (int i = 0; i < data.members.size(); i++)
    {
      int val = classify(data.members.get(i));
      if (val == 1)
      {
        if (data.members.get(i).classType == 1)
          tp += 1;
        else
          fp += 1;
      }
      else 
      {
        if (data.members.get(i).classType == 0)
          tn += 1;
        else
          fn += 1;
      }
    }
      double accuracy = ( (tp + tn) / (tp + tn + fp + fn) ) * 100;
      double precision = ( tp / (tp + fp) ) * 100;
      double recall = ( tp / (tp + fn) ) * 100;
      return new double[]{accuracy, precision, recall};
  }
  
  /**
   * Classifies the given data member.
   *
   * @param m The data member to classify.
   *
   * @return The predicted class value of the data member.
   */
  public int classify(DataMember m)
  {
    Node n = this.root;
    int i = 1;
    while(n.children.size() != 0)
    {
      n = n.children.get(m.attributeValues[n.attribute]);
      i++;
    }
    return n.classType;
  }
  
  /**
   * The copy constructor.
   */
  public ClassifierTree(ClassifierTree tree)
  {
    this.root = new Node(tree.root);
    copyHelper(this.root, tree.root);
  }
  
  /**
   * A helper method that recursively copies a tree structure.
   *
   * @param copy The node to which the original tree will be copied.
   *
   * @param orig The original tree to copy.
   */
  private void copyHelper(Node copy, Node orig)
  {
    if (orig.children.size() == 0)
      return;
    for(int i = 0; i < orig.children.size(); i++)
    {
      Node child = new Node(orig.children.get(i));
      copy.children.add(child);
      copyHelper(copy.children.get(i), orig.children.get(i));
    }
  }
}