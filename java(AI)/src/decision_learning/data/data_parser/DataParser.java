package decision_learning.data.data_parser;

import decision_learning.data.DataMember;
import decision_learning.data.DataSet;

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;

/**
 * An abstract class that provides parsing functionality not specific to a certain type of data.
 */
public abstract class DataParser{
  
  /*
   * The random number generator used to assign data members to data sets for testing or training.
   */
  Random rand = new Random();
  
  /**
   * The data members that a decision tree will be trained on.
   */
  protected ArrayList<DataMember> trainMembers = new ArrayList<DataMember>();
  
  /**
   * The data members that a decision tree will be tested on.
   */
  protected ArrayList<DataMember> testMembers = new ArrayList<DataMember>();
  
  /**
   * The proportion of data members parsed from the text file that will be assigned to the train set.
   */
  double trainPercentage= .8;
  
  /**
   * Parses data from a text file and randomly assigns data members to test and train sets.
   *
   * @param filename The name of the text file to parse from.
   */
  protected void getData(String filename) throws FileNotFoundException
  {
    Scanner file = new Scanner(new File(filename));
    while(file.hasNextLine())
    {
      if(rand.nextDouble() <= trainPercentage)
      {
        trainMembers.add(getDataMember(file.nextLine()));
      }
      else
      {
        testMembers.add(getDataMember(file.nextLine()));
      }
    }
  }
  
  /**
   * Parses a data member from a line from the text file. 
   * Converts all data values into integers which make it easer to train and test a decision tree.
   *
   * @param memberString The line from the text file which contains information relevant to one data member.
   *
   * @return The data member parsed from the input string.
   */
  public abstract DataMember getDataMember(String memberString);
  
  /**
   * Gets the set of data to train a decision tree on.
   *
   * @return The data set to train on.
   */
  public DataSet getTrainSet()
  {
    DataSet trainSet = new DataSet(trainMembers);
    return trainSet;
  }
  
  /**
   * Gets the set of data to test a decision tree on.
   *
   * @return The data set to test on.
   */
  public DataSet getTestSet()
  {
    DataSet testSet = new DataSet(testMembers);
    return testSet;
  }
}