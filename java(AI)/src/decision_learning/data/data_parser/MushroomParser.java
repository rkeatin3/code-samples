package decision_learning.data.data_parser;

import decision_learning.data.data_parser.DataParser;
import decision_learning.data.DataMember;

import java.io.FileNotFoundException;

/**
 * A parser class specifically created for the data file found <a href="http://archive.ics.uci.edu/ml/machine-learning-databases/mushroom/agaricus-lepiota.data">here</a.
 */ 
public class MushroomParser extends DataParser {
  
  /**
   * The class constructor. 
   *
   * @param filename The name of the file to parse from.
   */   
  public MushroomParser(String filename) throws FileNotFoundException
  {
    this.getData(filename);
  }
  
  public DataMember getDataMember(String memberString)  
  {
    DataMember c = new DataMember(22);
    c.classType = (memberString.charAt(0) == 'p')? 1 : 0;
    
    switch (memberString.charAt(2)) 
    {
      case 'b':
        c.attributeValues[0] = 0;
        break;
      case 'c':
        c.attributeValues[0] = 1;
        break;
      case 'x':
        c.attributeValues[0] = 2;
        break;
      case 'f':
        c.attributeValues[0] = 3;
        break;      
      case 'k':
        c.attributeValues[0] = 4;
        break;
      case 's':
        c.attributeValues[0] = 5;
        break;
    }
    switch (memberString.charAt(4)) 
    {
      case 'f':
        c.attributeValues[1] = 0;
        break;
      case 'g':
        c.attributeValues[1] = 1;
        break;
      case 'y':
        c.attributeValues[1] = 2;
        break;
      case 's':
        c.attributeValues[1] = 3;
    }    
    switch (memberString.charAt(6)) 
    {
      case 'n':
        c.attributeValues[2] = 0;
        break;
      case 'b':
        c.attributeValues[2] = 1;
        break;
      case 'c':
        c.attributeValues[2] = 2;
        break;
      case 'g':
        c.attributeValues[2] = 3;
        break;      
      case 'r':
        c.attributeValues[2] = 4;
        break;
      case 'p':
        c.attributeValues[2] = 5;
        break;
      case 'u':
        c.attributeValues[2] = 6;
        break;
      case 'e':
        c.attributeValues[2] = 7;
        break;      
      case 'w':
        c.attributeValues[2] = 8;
        break;
      case 'y':
        c.attributeValues[2] = 9;
        break;
    }
    switch (memberString.charAt(8)) 
    {
      case 't':
        c.attributeValues[3] = 0;
        break;
      case 'f':
        c.attributeValues[3] = 1;
        break;
    }
    switch (memberString.charAt(10)) 
    {
      case 'a':
        c.attributeValues[4] = 0;
        break;
      case 'l':
        c.attributeValues[4] = 1;
        break;
      case 'c':
        c.attributeValues[4] = 2;
        break;
      case 'y':
        c.attributeValues[4] = 3;
        break;      
      case 'f':
        c.attributeValues[4] = 4;
        break;
      case 'm':
        c.attributeValues[4] = 5;
        break;
      case 'n':
        c.attributeValues[4] = 6;
        break;
      case 'p':
        c.attributeValues[4] = 7;
        break;      
      case 's':
        c.attributeValues[4] = 8;
        break;
    }
    switch (memberString.charAt(12)) 
    {
      case 'a':
        c.attributeValues[5] = 0;
        break;
      case 'd':
        c.attributeValues[5] = 1;
        break;
      case 'f':
        c.attributeValues[5] = 2;
        break;
      case 'n':
        c.attributeValues[5] = 3;
        break;
    }
    switch (memberString.charAt(14)) 
    {
      case 'c':
        c.attributeValues[6] = 0;
        break;
      case 'w':
        c.attributeValues[6] = 1;
        break;
      case 'd':
        c.attributeValues[6] = 2;
        break;
    }
    switch (memberString.charAt(16)) 
    {
      case 'b':
        c.attributeValues[7] = 0;
        break;
      case 'n':
        c.attributeValues[7] = 1;
        break;
    }
    switch (memberString.charAt(18)) 
    {
      case 'k':
        c.attributeValues[8] = 0;
        break;
      case 'n':
        c.attributeValues[8] = 1;
        break;
      case 'b':
        c.attributeValues[8] = 2;
        break;
      case 'h':
        c.attributeValues[8] = 3;
        break;      
      case 'g':
        c.attributeValues[8] = 4;
        break;
      case 'r':
        c.attributeValues[8] = 5;
        break;
      case 'o':
        c.attributeValues[8] = 6;
        break;
      case 'p':
        c.attributeValues[8] = 7;
        break;      
      case 'u':
        c.attributeValues[8] = 8;
        break;
      case 'e':
        c.attributeValues[8] = 9;
        break;
      case 'w':
        c.attributeValues[8] = 10;
        break;      
      case 'y':
        c.attributeValues[8] = 11;
        break;
    }
    switch (memberString.charAt(20)) 
    {
      case 'e':
        c.attributeValues[9] = 0;
        break;
      case 't':
        c.attributeValues[9] = 1;
        break;
    }
    switch (memberString.charAt(22)) 
    {
      case 'b':
        c.attributeValues[10] = 0;
        break;
      case 'c':
        c.attributeValues[10] = 1;
        break;
      case 'u':
        c.attributeValues[10] = 2;
        break;
      case 'e':
        c.attributeValues[10] = 3;
        break;      
      case 'z':
        c.attributeValues[10] = 4;
        break;
      case 'r':
        c.attributeValues[10] = 5;
        break;
      case '?':
        c.attributeValues[10] = 6;
        break; 
    }
    switch (memberString.charAt(24)) 
    {
      case 'f':
        c.attributeValues[11] = 0;
        break;
      case 'y':
        c.attributeValues[11] = 1;
        break;
      case 'k':
        c.attributeValues[11] = 2;
        break;
      case 's':
        c.attributeValues[11] = 3;
        break; 
    }
    switch (memberString.charAt(26)) 
    {
      case 'f':
        c.attributeValues[12] = 0;
        break;
      case 'y':
        c.attributeValues[12] = 1;
        break;
      case 'k':
        c.attributeValues[12] = 2;
        break;
      case 's':
        c.attributeValues[12] = 3;
        break; 
    }
    switch (memberString.charAt(28)) 
    {
      case 'n':
        c.attributeValues[13] = 0;
        break;
      case 'b':
        c.attributeValues[13] = 1;
        break;
      case 'c':
        c.attributeValues[13] = 2;
        break;
      case 'g':
        c.attributeValues[13] = 3;
        break;      
      case 'o':
        c.attributeValues[13] = 4;
        break;
      case 'p':
        c.attributeValues[13] = 5;
        break;
      case 'e':
        c.attributeValues[13] = 6;
        break;
      case 'w':
        c.attributeValues[13] = 7;
        break;      
      case 'y':
        c.attributeValues[13] = 8;
    }
    switch (memberString.charAt(30)) 
    {
      case 'n':
        c.attributeValues[14] = 0;
        break;
      case 'b':
        c.attributeValues[14] = 1;
        break;
      case 'c':
        c.attributeValues[14] = 2;
        break;
      case 'g':
        c.attributeValues[14] = 3;
        break;      
      case 'o':
        c.attributeValues[14] = 4;
        break;
      case 'p':
        c.attributeValues[14] = 5;
        break;
      case 'e':
        c.attributeValues[14] = 6;
        break;
      case 'w':
        c.attributeValues[14] = 7;
        break;      
      case 'y':
        c.attributeValues[14] = 8;
    }
    switch (memberString.charAt(32)) 
    {
      case 'p':
        c.attributeValues[15] = 0;
        break;
      case 'u':
        c.attributeValues[15] = 1;
        break;
    }
    switch (memberString.charAt(34)) 
    {
      case 'n':
        c.attributeValues[16] = 0;
        break;
      case 'o':
        c.attributeValues[16] = 1;
        break;
      case 'w':
        c.attributeValues[16] = 2;
        break;
      case 'y':
        c.attributeValues[16] = 3;
        break;      
    }
    switch (memberString.charAt(36)) 
    {
      case 'n':
        c.attributeValues[17] = 0;
        break;
      case 'o':
        c.attributeValues[17] = 1;
        break;
      case 't':
        c.attributeValues[17] = 2;
        break;
    }
    switch (memberString.charAt(38)) 
    {
      case 'c':
        c.attributeValues[18] = 0;
        break;
      case 'e':
        c.attributeValues[18] = 1;
        break;
      case 'f':
        c.attributeValues[18] = 2;
        break;
      case 'l':
        c.attributeValues[18] = 3;
        break;      
      case 'n':
        c.attributeValues[18] = 4;
        break;
      case 'p':
        c.attributeValues[18] = 5;
        break;
      case 's':
        c.attributeValues[18] = 6;
        break;
      case 'z':
        c.attributeValues[18] = 7;
        break;      
    }
    switch (memberString.charAt(40)) 
    {
      case 'k':
        c.attributeValues[19] = 0;
        break;
      case 'n':
        c.attributeValues[19] = 1;
        break;
      case 'b':
        c.attributeValues[19] = 2;
        break;
      case 'h':
        c.attributeValues[19] = 3;
        break;      
      case 'r':
        c.attributeValues[19] = 4;
        break;
      case 'o':
        c.attributeValues[19] = 5;
        break;
      case 'u':
        c.attributeValues[19] = 6;
        break;
      case 'w':
        c.attributeValues[19] = 7;
        break;      
      case 'y':
        c.attributeValues[19] = 8;
    }
    switch (memberString.charAt(42)) 
    {
      case 'a':
        c.attributeValues[20] = 0;
        break;
      case 'c':
        c.attributeValues[20] = 1;
        break;
      case 'n':
        c.attributeValues[20] = 2;
        break;
      case 's':
        c.attributeValues[20] = 3;
        break;      
      case 'v':
        c.attributeValues[20] = 4;
        break;
      case 'y':
        c.attributeValues[20] = 5;
        break;
    }
    switch (memberString.charAt(44)) 
    {
      case 'g':
        c.attributeValues[21] = 0;
        break;
      case 'l':
        c.attributeValues[21] = 1;
        break;
      case 'm':
        c.attributeValues[21] = 2;
        break;
      case 'p':
        c.attributeValues[21] = 3;
        break;      
      case 'u':
        c.attributeValues[21] = 4;
        break;
      case 'w':
        c.attributeValues[21] = 5;
        break;
      case 'd':
        c.attributeValues[21] = 6;
        break;
    }
    return c;
  }
}