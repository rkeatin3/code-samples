package decision_learning.data.data_parser;

import decision_learning.data.data_parser.DataParser;
import decision_learning.data.DataSet;
import decision_learning.data.DataMember;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;

/**
 * A parser class specifically created for the data files found <a href="http://archive.ics.uci.edu/ml/machine-learning-databases/monks-problems/">here</a.
 */ 
public class MonkParser extends DataParser {
  
  ArrayList<DataMember> trainMembers = new ArrayList<DataMember>();
  ArrayList<DataMember> testMembers = new ArrayList<DataMember>();
  
  /**
   * The class constructor. Calls methods which parse the train and test sets from their separate files. 
   *
   * @param filename The name of the data set to parse from.
   */   
  public MonkParser(String filename) throws FileNotFoundException
  {
    this.setTrainSet(filename);
    this.setTestSet(filename);
  }
  
  /**
   * Parses the train set.
   * 
   * @param filename The name of the data set to parse from.
   */
  private void setTrainSet(String filename) throws FileNotFoundException
  {
    Scanner file = new Scanner(new File(filename + ".train.txt"));
    while(file.hasNextLine())
    {
      trainMembers.add(getDataMember(file.nextLine()));
    }
  }
  
  /**
   * Parses the test set.
   * 
   * @param filename The name of the data set to parse from.
   */
  private void setTestSet(String filename) throws FileNotFoundException
  {
    Scanner file = new Scanner(new File(filename + ".test.txt"));
    while(file.hasNextLine())
    {
      testMembers.add(getDataMember(file.nextLine()));
    }
  }
  
  public DataMember getDataMember(String memberString)  
  {
    DataMember c = new DataMember(6);
    c.classType = Character.getNumericValue(memberString.charAt(1));
    for (int i = 0; i < c.attributeValues.length; i++)
    {
      c.attributeValues[i] = Character.getNumericValue(memberString.charAt(3 + i * 2)) - 1;
    }
    return c;
  }
  
  public DataSet getTrainSet()
  {
    DataSet trainSet = new DataSet(trainMembers);
    return trainSet;
  }
  
  public DataSet getTestSet()
  {
    DataSet testSet = new DataSet(testMembers);
    return testSet;
  }
}

















