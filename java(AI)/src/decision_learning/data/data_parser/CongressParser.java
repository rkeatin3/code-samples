package decision_learning.data.data_parser;

import decision_learning.data.data_parser.DataParser;
import decision_learning.data.DataMember;


import java.io.FileNotFoundException;

/**
 * A parser class specifically created for the data file found <a href="http://archive.ics.uci.edu/ml/machine-learning-databases/voting-records/house-votes-84.data">here</a.
 */ 
public class CongressParser extends DataParser {
  
  /**
   * The class constructor. 
   *
   * @param filename The name of the file to parse from.
   */   
  public CongressParser(String filename) throws FileNotFoundException
  {
    this.getData(filename);
  }
  
  public DataMember getDataMember(String memberString)  
  {
    DataMember c = new DataMember(16);
    int ind = memberString.indexOf(',');
    c.classType = (memberString.substring(0,ind).equals("democrat"))? 1 : 0;
    for (int i = 0; i <c.attributeValues.length; i++)
    {
      if(memberString.charAt(ind+1+i*2) == 'n')
      {
        c.attributeValues[i] = 0;
      }
      else if (memberString.charAt(ind+1+i*2) == 'y') 
      {
        c.attributeValues[i] = 1;
      }
      else
      {
        c.attributeValues[i] = 2;
      }
    }
    return c;
  }
}