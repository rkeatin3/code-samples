package decision_learning.data;

/**
 * A class that implements a data member for a data set
 */
public class DataMember {
  public int classType;
  public int[] attributeValues;
  
  public DataMember(int numAttributes)
  {
    attributeValues = new int[numAttributes];
  }
}