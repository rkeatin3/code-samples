package decision_learning.data;

import decision_learning.data.DataMember;

import java.util.ArrayList;

/**
 * A class that holds a set of data members and information about the values they can take on.
 */
public class DataSet {
  
  /**
   * The set of data members.
   */
  public ArrayList<DataMember> members;
  
  /**
   * An array that indicates the number of possible values for each attribute the data members have.
   */
  public int[] attributePossValues;
  
  /**
   * The class constructor.
   *
   * @param members The data members to include in the data set.
   */
  public DataSet(ArrayList<DataMember> members)
  {
    this.members = members;
    this.initialize();
  }
  
  /**
   * Uses the first data member to determine whether the data belongs to the monks, mushroom, or congressional voting data sets.
   * Then initializes attributePossValues to contain the correct values.
   */
  private void initialize()
  {
    switch (members.get(0).attributeValues.length) 
    {
      case 16:
        attributePossValues = new int[] {3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3};
        break;
      case 22:
        attributePossValues = new int[] {6,4,10,2,9,4,3,2,12,2,7,4,4,9,9,2,4,3,8,9,6,7};
        break;
      case 6:
        attributePossValues = new int[] {3,3,2,3,4,2};
        break;
    }
  }
}