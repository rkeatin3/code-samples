package decision_learning;

import java.util.Scanner;
import java.io.FileNotFoundException;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.FitnessFunction;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.Precision;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.PrecisionRecallCombination;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.fitness_function.Recall;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy.ReplacementStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy.Elitism;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.replacement_strategy.TotalReplacement;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.SelectionStrategy;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.FitnessProportionate;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.RankBased;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.selection_strategy.TournamentBased;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.TerminationCondition;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.GenerationsWithoutImprovement;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.GenerationLimit;
import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.termination_condition.TimeLimit;

import decision_learning.classifier_tree.learning_algorithm.genetic_algorithm.GeneticAlgorithm;

import decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion.AttributeCriterion;
import decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion.GainRatio;
import decision_learning.classifier_tree.learning_algorithm.traditional.attribute_criterion.InformationGain;

import decision_learning.classifier_tree.learning_algorithm.traditional.Traditional;

import decision_learning.classifier_tree.learning_algorithm.LearningAlgorithm;

import decision_learning.classifier_tree.ClassifierTree;

import decision_learning.data.data_parser.DataParser;
import decision_learning.data.data_parser.CongressParser;
import decision_learning.data.data_parser.MonkParser;
import decision_learning.data.data_parser.MushroomParser;

import decision_learning.data.DataSet;


class Classifier {
  
  static Scanner kb = new Scanner(System.in);
  
  public static void main(String[] args) throws FileNotFoundException{
    System.out.println("Please enter the value for the desired data set:");
    System.out.println("\t1. Congressional Voting Data");
    System.out.println("\t2. Mushroom Data");
    System.out.println("\t3. Monks-1");
    System.out.println("\t4. Monks-2");
    System.out.println("\t5. Monks-3");
    int parserInt = kb.nextInt();
    DataParser p;
    switch (parserInt)
    {
      case 1:
        System.out.println("You chose Congressional Voting Data.");
        p = new CongressParser("../data/house-votes-84.txt");
        break;
      case 2:
        System.out.println("You chose Mushroom Data.");
        p = new MushroomParser("../data/agaricus-lepiota.txt");
        break;
      case 3:
        System.out.println("You chose Monks-1.");
        p = new MonkParser("../data/monks-1");
        break;
      case 4:
        System.out.println("You chose Monks-2.");
        p = new MonkParser("../data/monks-2");
        break;
      case 5:
        System.out.println("You chose Monks-3.");
        p = new MonkParser("../data/monks-2");
        break;
      default:
        //Dummy creation to avoid "error: variable p might not have been initialized"
        p = new MonkParser("../data/monks-2");
        System.out.println("Your input was not valid.");
        System.exit(1);
    }
    
    DataSet trainSet = p.getTrainSet();
    
//    System.out.println("attributePossValues is ");
//    for (int i = 0; i < trainSet.attributePossValues.length; i++)
//    {
//      System.out.print(trainSet.attributePossValues[i]);
//    }
//    System.out.println();
//    for(int i = 0; i < trainSet.members.size(); i++)
//    {
//      System.out.println("classtype is" + trainSet.members.get(i).classType);
//      System.out.print("attributeValues is ");
//      for (int j = 0; j < trainSet.members.get(i).attributeValues.length; j++)
//      {
//        System.out.print(trainSet.members.get(i).attributeValues[j]);
//      }
//      System.out.println();
//    }
    
    DataSet testSet = p.getTestSet();
    
    System.out.println("Please enter the value for type of learning algorithm:");
    System.out.println("\t1. Traditional");
    System.out.println("\t2. Genetic");
    int algInt = kb.nextInt();
    
    //Dummy creation to avoid "error: variable a might not have been initialized"
    LearningAlgorithm a = new Traditional(new InformationGain(), trainSet);
    if(algInt != 1 && algInt != 2)
    {
      System.out.println("Your input was not valid.");
      System.exit(1);
    }
    else if(algInt == 1)
    {
      System.out.println("You chose a traditional learning algorithm.");
      System.out.println("Please enter the value for the desired fitness criterion:");
      System.out.println("\t1. Information gain");
      System.out.println("\t2. Gain ratio");
      int fitInt = kb.nextInt();
      if(fitInt != 1 && fitInt != 2)
      {
        System.out.println("Your input was not valid.");
        System.exit(1);
      }
      switch (fitInt)
      {
        case 1:
          System.out.println("You chose to use information gain as the fitness criterion.");
          a = new Traditional(new InformationGain(), trainSet);
          break;
        case 2:
          System.out.println("You chose to use gain ratio as the fitness criterion.");
          a = new Traditional(new GainRatio(), trainSet);
          break;
      }
    }
    else
    {
      System.out.println("You chose to use a genetic learning algorithm");
      System.out.println("Please enter the value for the population size:");
      int popSize = kb.nextInt();
      System.out.println("Please enter the value for the fitness function");
      System.out.println("\t1. Precision");
      System.out.println("\t2. Recall");
      System.out.println("\t3. Average of precision and recall");
      int fitInt = kb.nextInt();
      if(fitInt != 1 && fitInt != 2 && fitInt != 3)
      {
        System.out.println("Your input was not valid.");
        System.exit(1);
      }
      FitnessFunction fit;
      switch (fitInt)
      {
        case 1:
          System.out.println("You chose to use precision as the fitness function");
          fit = new Precision(trainSet);
          break;
        case 2:
          System.out.println("You chose to use recall as the fitness function");
          fit = new Recall(trainSet);
          break;
        case 3:
          System.out.println("You chose to use the average of precision and recall as the fitness function");
          fit = new PrecisionRecallCombination(.5, trainSet);
          break;
        default:
          //Dummy creation to avoid "error: variable p might not have been initialized"
          fit = new PrecisionRecallCombination(.5, trainSet);
          System.out.println("Your input was not valid.");
          System.exit(1);
      }
      
      
//      System.out.println("Please enter the value for the desired selection strategy:");
//      System.out.println("\t1. Fitness proportionate based");
//      System.out.println("\t2. Rank based");
//      System.out.println("\t3. Tournament based");
//      int selInt = kb.nextInt();
      int selInt = 1;
      SelectionStrategy sel;
      switch (selInt)
      {
        case 1:
          System.out.println("You chose to use a fitness proportionate based replacement strategy");
          sel = new FitnessProportionate(fit);
          break;
        case 2:
          System.out.println("You chose to use a rank based replacement strategy");
          sel = new RankBased(fit);
          break;
        case 3:
          System.out.println("You chose to use a tournament proportionate based replacement strategy");
          sel = new TournamentBased(fit, 10);
          break; 
        default:
          //Dummy creation to avoid "error: variable p might not have been initialized"
          sel = new TournamentBased(fit, 10);
          System.out.println("Your input was not valid.");
          System.exit(1);
      }

      
      System.out.println("Please enter the value for the termination condition:");
      System.out.println("\t1. Generation limit");
      System.out.println("\t2. Time limit");
      System.out.println("\t3. Limit on number of generations without improvement");
      int termInt = kb.nextInt();
      int timeLim = 0;
      int genLim = 0;
      if(termInt != 1 && termInt != 2 && termInt!= 3)
      {
        System.out.println("Your input was not valid.");
        System.exit(1);
      }
      if (termInt == 1)
      {
        System.out.println("Please enter the limit on generations:");
        genLim = kb.nextInt();
      }
      if (termInt == 2)
      {
        System.out.println("Please enter the limit on time in seconds:");
        timeLim = kb.nextInt();
      }
      if (termInt == 3)
      {
        System.out.println("Please enter the limit on generations without improvement:");
        genLim = kb.nextInt();
      }
      //Dummy creation to avoid "error: variable term might not have been initialized"
      TerminationCondition term = new GenerationLimit(genLim);
      switch(termInt)
      {
        case 1:
          System.out.println("You chose to use a generation limit of " + genLim);
          term = new GenerationLimit(genLim);
          break;
        case 2:
          System.out.println("You chose to use a time limit of " + timeLim);
          term = new TimeLimit(timeLim);
          break;
        case 3:
          System.out.println("You chose to use a time limit of " + timeLim);
          term = new GenerationsWithoutImprovement(genLim, fit);
          break;
      }
      
      System.out.println("Please enter the value for the replacement strategy:");
      System.out.println("\t1. Total replacement");
      System.out.println("\t2. Elitism");
      int repInt = kb.nextInt();
      if (repInt != 1 && repInt != 2)
      {
        System.out.println("Your input was not valid.");
        System.exit(1);
      }
      int numElite = 0;
      if(repInt == 2)
      {
        System.out.println("Please enter the number of elite members to preserve each generation:");
         numElite = kb.nextInt();
      }
      
      ReplacementStrategy rep = new TotalReplacement(trainSet.attributePossValues);
      switch (repInt)
      {
        case 1:
          rep = new TotalReplacement(trainSet.attributePossValues);
          break;
        case 2:
          rep = new Elitism(fit, numElite, trainSet.attributePossValues);
          break;
      }    
      a = new GeneticAlgorithm(popSize, term, sel, rep, trainSet);
    }
    
    long startTrain = System.currentTimeMillis();
    ClassifierTree t = new ClassifierTree(a.learnTree());
    System.out.println("Trained for " + (System.currentTimeMillis() - startTrain)/1000.0 + " Seconds");
    double[] metrics = t.getTestMetrics(testSet);
    System.out.printf("The accuracy of the learned decision tree on the test set was %.2f\n", metrics[0]);
    System.out.printf("The precision of the learned decision tree on the test set was %.2f\n", metrics[1]);
    System.out.printf("The recall of the learned decision tree on the test set was %.2f\n", metrics[2]);
  }
}