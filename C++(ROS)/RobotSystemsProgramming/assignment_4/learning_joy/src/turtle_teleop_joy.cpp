// joy teleop turtlesim example 2014-02-19 LLW
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <unistd.h>

// global cmd vel twist
static geometry_msgs::Twist command_velocity;

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  // X vel driven by left joystick for and aft
  command_velocity.linear.x  = 2.0*joy->axes[1];
  // Y vel driven by left joystick left and right
  command_velocity.linear.y  = 2.0*joy->axes[0];
  // Z vel driven by right and left triggers
  command_velocity.linear.z = joy->axes[2]-joy->axes[5];

  // roll vel driven by right joystick left and right
  command_velocity.angular.x  = -2.0*joy->axes[3];
  // pitch vel driven by right joystick for and and aft
  command_velocity.angular.y = 2.0*joy->axes[4];
  // yaw vel driven by right and left bumpers
  command_velocity.angular.z = 2.0*(joy->buttons[4]-joy->buttons[5]);

}


int main(int argc, char** argv)
{

  // init ros
  ros::init(argc, argv, "turtle_teleop_joy");

  // create node handle
  ros::NodeHandle node;

  // advertise topic that this node will publish
  ros::Publisher  turtle_vel =
    node.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 10);

  // subcscribe to joy topic
  ros::Subscriber sub = node.subscribe("joy", 10, &joyCallback);

  ros::Rate rate(10);

  while (node.ok())
    {
      // publish the cmd vel
      turtle_vel.publish(command_velocity);
 
      // spin
      ros::spinOnce();

      // wait 50 ms
      rate.sleep();

    }

  return 0;

};
