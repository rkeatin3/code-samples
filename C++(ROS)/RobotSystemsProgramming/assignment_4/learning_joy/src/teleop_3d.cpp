#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <unistd.h>
#include <tf/transform_broadcaster.h>
#include <Eigen/Core>
#include <unsupported/Eigen/MatrixFunctions>

void twistCallback(const geometry_msgs::Twist::ConstPtr& cmd_vel)
{
  //Creating a persistent broadcaster
  static tf::TransformBroadcaster br;
  //Creating a persistent frame for the turtle
  static tf::Transform turtle3d_frame = tf::Transform::getIdentity();
  
  //Creating a frame which will be used to move turtle from last position
  tf::Transform turtle3d_frame_delta;
  
  //Creating persistent variables for measuring time interval between calls
  static ros::Time now = ros::Time::now();
  static ros::Time last_time;
  //Creating a duration variable to measure the time difference
  ros::Duration delta_t;
  
  //Updating times and 
  last_time = now;
  now = ros::Time::now();
  delta_t = now - last_time;

  //Creating matrices for rotation and skew(ang_vel)
  Eigen::Matrix3d R,J;
  //Creating a tf vector for the frame translation
  tf::Vector3 xyz_translation = tf::Vector3(cmd_vel->linear.x*delta_t.toSec(),
					    cmd_vel->linear.y*delta_t.toSec(),
					    cmd_vel->linear.z*delta_t.toSec());
  
  //Setting the translational component of turtle3d_frame_delta
  turtle3d_frame_delta.setOrigin(xyz_translation);

  //Initialize J as the skew of the angular velocity vector
  J <<        0          , -cmd_vel->angular.z, cmd_vel->angular.y,
       cmd_vel->angular.z,         0          , -cmd_vel->angular.x,
      -cmd_vel->angular.y,  cmd_vel->angular.x,        0;

  //Initialize R as the 3x3 identity matrix
  R = Eigen::Matrix3d::Identity();

  //Calculating R=expm(J*t)
  J = J*delta_t.toSec();
  R = J.exp();

  //Setting the rotation portion of  turtle3d_frame_delta to R
  turtle3d_frame_delta.setBasis(tf::Matrix3x3(R(0,0), R(0,1), R(0,2),
					      R(1,0), R(1,1), R(1,2),
					      R(2,0), R(2,1), R(2,2)));

  //Integrating the transformation
  turtle3d_frame =  turtle3d_frame * turtle3d_frame_delta;

  //Broadcasting the transform
  br.sendTransform(tf::StampedTransform(turtle3d_frame, ros::Time::now(), "world","turtle3d"));
}

int main(int argc, char** argv){

   // init ros
  ros::init(argc, argv, "teleop_3d");

  // create node handle
  ros::NodeHandle node;

  // subcscribe to 
  ros::Subscriber sub = node.subscribe("/turtle1/cmd_vel", 10, &twistCallback);

  //publishing at 20 Hz
  ros::Rate rate(20);

  while(node.ok())
    {
      //spin
      ros::spinOnce();

      // wait 50 ms
      rate.sleep();
    }

  return 0;
};
