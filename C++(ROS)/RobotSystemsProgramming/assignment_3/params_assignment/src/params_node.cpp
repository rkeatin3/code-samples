#include <ros/ros.h>
#include "std_msgs/String.h"
#include <assert.h>

int main(int argc, char **argv)
{

  ros::init(argc, argv, "param_user");

  ros::NodeHandle n;

  int int1, int2, sum, sum_checker;
  sum_checker = 0;

  if (n.getParam("param1",int1) && n.getParam("param2",int2))
    {
      sum = int1 + int2;
      n.setParam("sum_param",sum);
    }
  n.getParam("sum_param",sum_checker);
  assert(sum_checker  == int1 + int2);
}
      
      
				
