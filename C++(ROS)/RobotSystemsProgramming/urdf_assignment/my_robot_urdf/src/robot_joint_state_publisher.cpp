#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/Joy.h>
#include <tf/transform_broadcaster.h>
#include <math.h>

static ros::Publisher pb;
static float joints[6];

void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{  
  joints[0] = 3.14159*joy->axes[0];
  joints[1] = 3.14159*joy->axes[1];
  joints[2] = 3.14159*joy->axes[4];
  joints[3] = 3.14159*joy->axes[3];
  joints[4] = 3.14159*(joy->axes[5])-3.14159;
  joints[5] = 3.14159*(joy->axes[2])-3.14159;
  
}

int main(int argc, char** argv){

  // init ros
  ros::init(argc, argv, "my_robot_stuff");

  // create node handle
  ros::NodeHandle node;

  // advertise topic that this node will publish
  pb = node.advertise<sensor_msgs::JointState>("joint_states", 10);

  // subcscribe to joy topic
  ros::Subscriber sub = node.subscribe("joy", 10, &joyCallback);


  ros::Rate rate(100);

  //Creating a persistent broadcaster
  static tf::TransformBroadcaster br;
  //Creating a persistent frame for the turtle
  static tf::Transform robot_base = tf::Transform::getIdentity();
  //Defining an angle to automatically move robot_base
  static float theta = 0;

  while(ros::ok())
  {
      //spin
      ros::spinOnce();

      // publish
      sensor_msgs::JointState FANUC_joints =  sensor_msgs::JointState();
      FANUC_joints.position.resize(6);
      FANUC_joints.name.resize(6);
      FANUC_joints.name[0] = "waist";
      FANUC_joints.position[0] = joints[0];
      FANUC_joints.name[1] = "shoulder";
      FANUC_joints.position[1] = joints[1];
      FANUC_joints.name[2] = "elbow";
      FANUC_joints.position[2] = joints[2];
      FANUC_joints.name[3] = "wrist1";
      FANUC_joints.position[3] = joints[3];
      FANUC_joints.name[4] = "wrist2";
      FANUC_joints.position[4] = joints[4];
      FANUC_joints.name[5] = "wrist3";
      FANUC_joints.position[5] = joints[5];
      FANUC_joints.header.stamp = ros::Time::now();
      pb.publish(FANUC_joints);
      //Broadcasting the transform
      br.sendTransform(tf::StampedTransform(robot_base, ros::Time::now(), "world","robot_base"));

      

       //Defining xyz vector for robot_base
      tf::Vector3 xyz =  tf::Vector3(cos(theta),0.0,0.0);
      robot_base.setOrigin(xyz);
      
      //Incrementing theta
      theta+=0.01;

      rate.sleep();
  }

  return 0;
}
