#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <sensor_msgs/Joy.h>
#include <math.h>
#include <Eigen/Core>
#include <unsupported/Eigen/MatrixFunctions>

#define PI 3.1415926
static const float a[6] = {.95,0,0,1.25,0,.27};
static const float d[6] = {.37,1.05,.2,0,0,0};
static const float alpha[6] = {PI/2,0,PI/2,-PI/2,PI/2,0};
static float theta[6];


void joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{  

}

void M900INV(Eigen::Matrix3d T06)
{
  Eigen::Vector3d X6Wrist(0,0,-.27);
  Eigen::Vector3d X0Wrist = T06*X6Wrist;
}

void THREEINV(Eigen::Vector3d X)
{
  theta[0] = atan2(X[1],X[0]);
  float l1 = a[1];
  float l2 = sqrt(pow(a[2],2)+pow(d[3],2));
  float y1w = X[2] - d[0];
  float x1w = sqrt(pow(X[0],2)+pow(X[1],2))-a[0];
  float d14 = sqrt(pow(y1w,2)+pow(x1w,2));
  float d14max = l1 + l2;
  float d14min = abs(l1 - l2);

  if (d14 > d14max || d14 < d14min)
    return;
  else if(d14 == d14max)
  {
    theta[1] = atan2(y1w,x1w);
    theta[2] = atan2(d[3],a[2]);
    return;
  }
  else if(abs(d14 - d14min) < .0001)
  {
    theta[1] = atan2(y1w,x1w) + PI;
    theta[2]= PI + atan2(d[3],a[2]);
    return;
  }

  float alpha = atan2(y1w,x1w);
  float beta = acos( ( pow(l2,2) -pow(l1,2) - pow(x1w,2) - pow(y1w,2) ) /(-2*l1*d14) );
  float gamma = acos( ( pow(x1w,2) + pow(y1w,2) - pow(l1,2) - pow(l2,2) ) / (-2*l1*l2) );
  if( x1w > 0)
  {
    theta[1] = alpha + beta;
    theta[2] = gamma + PI + atan(d[3]/a[2]);
  }
  else        
    theta[1] = alpha - beta;
    theta[2] = PI - gamma + atan2(d[3],a[2]); 
}

int main(int argc, char** argv){

  // init ros
  ros::init(argc, argv, "my_robot_stuff");

  // create node handle
  ros::NodeHandle node;

  // advertise topic that this node will publish
  ros::Publisher pb = node.advertise<sensor_msgs::JointState>("joint_states", 10);

  // subcscribe to joy topic
  ros::Subscriber sub = node.subscribe("joy", 10, &joyCallback);

  ros::Rate rate(100);

  while(ros::ok())
  {
      //spin
      ros::spinOnce();

      // publish
      sensor_msgs::JointState FANUC_joints =  sensor_msgs::JointState();
      FANUC_joints.position.resize(6);
      FANUC_joints.name.resize(6);
      FANUC_joints.name[0] = "waist";
      FANUC_joints.position[0] = theta[0];
      FANUC_joints.name[1] = "shoulder";
      FANUC_joints.position[1] = theta[1];
      FANUC_joints.name[2] = "elbow";
      FANUC_joints.position[2] = theta[2];
      FANUC_joints.name[3] = "wrist1";
      FANUC_joints.position[3] = theta[3];
      FANUC_joints.name[4] = "wrist2";
      FANUC_joints.position[4] = theta[4];
      FANUC_joints.name[5] = "wrist3";
      FANUC_joints.position[5] = theta[5];
      FANUC_joints.header.stamp = ros::Time::now();
      pb.publish(FANUC_joints);
      rate.sleep();
  }

  return 0;
}

