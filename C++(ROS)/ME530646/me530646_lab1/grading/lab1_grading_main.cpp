#include <Eigen/Core>
#include <me530646_lab1/lab1.h>
#include <ur5/utilities.h>
#include <math.h>
#include <iostream>
#include <ros/ros.h>

#define PI 3.1415926

int main(int argc, char **argv){
	//Initializing ROS
    ros::init(argc, argv, "lab1_main");
    ros::NodeHandle node;
    //Creating an RvizPlotter
	RvizPlotter p = RvizPlotter(node);
	
	printf("Testing rollr\n");
	printEigen(rollr(PI/2.1));
	printEigen(rollr(-PI/.5));
	
	printf("Testing pitchr\n");
	printEigen(pitchr(PI/2.7));
	printEigen(pitchr(-PI/4.2));

	printf("Testing yawr\n");
	printEigen(yawr(PI/3.7));
	printEigen(yawr(-PI/1.2));

	printf("Testing rpyr\n");
	printEigen(rpyr(PI/3.7,PI/2.1,-PI/4.8));
	printEigen(rpyr(-PI/1.2,PI/2,PI/5.3));

	printf("Testing rpyrinv\n");
	Eigen::Vector3f q1 = rpyrinv(rpyr(-PI/2.1,PI/3.7,PI/.3));
	Eigen::Vector3f q2 = rpyrinv(rpyr(PI/5.3,PI/4.2,-PI/1.9));
	printEigen(rpyr(q1(0),q1(1),q1(2)));
	printEigen(rpyr(q2(0),q2(1),q2(2)));

	printf("Testing xf\n");
	printEigen(xf(1,2,-2,PI/2.1,-PI/.3,PI/7.2));
	printEigen(xf(0,-4,.7,PI/3.2,-PI/3,-PI/5.2));

	printf("Testing finv\n");
	Eigen::MatrixXf xf1 = xf(-2,0,1,PI/1.2,PI/4.7,-PI/.8);
	Eigen::MatrixXf xf2 = xf(3.2,-3.8,-.9,PI/2.3,-PI/7.3,PI/6.6);
	printEigen(finv(xf1)*xf1);
	printEigen(finv(xf2)*xf2);

	ZYXRelativePlot(p, PI/2.4, PI/3.1, PI/4.2);
	XYZFixedPlot(p, PI/2.4, PI/3.1, PI/4.2);
}