#ifndef LAB1_H
#define LAB1_H

#include <Eigen/Core>
#include <ur5/utilities.h>

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the x axis (roll) of phi radians.
 */
Eigen::Matrix3d rollr(double phi);

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the y axis (pitch) of theta radians.
 */
Eigen::Matrix3d pitchr(double theta);

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the z axis (yaw) of psi radians.
 */
Eigen::Matrix3d yawr(double psi);

/**
 * Returns the 3x3 rotation matrix that represents successive 
 * roll, pitch, and yaw rotations.
 */
Eigen::Matrix3d rpyr(const Eigen::Vector3d &rpy);

/**
 * Returns the roll, pitch, and yaw of the given 3x3 rotation matrix.
 */
Eigen::Vector3d rpyrinv(const Eigen::Matrix3d &rpyr);

/**
 * Returns the 4x4 homogeneous transformation that represents a 
 * translation of (x,y,z) and a rotation of (roll,pitch,yaw).
 */
Eigen::Matrix4d xf(const Eigen::VectorXd &xyzrpy);

/**
 * Returns the matrix inverse to the given homogeneous transformation.
 */
Eigen::Matrix4d finv(const Eigen::Matrix4d &f);

/**
 * Animates a roll-pitch-yaw rotation about the world
 * coordinate frame
 */
void XYZFixedPlot(RvizPlotter &plotter,const Eigen::Vector3d &rpy);

/**
 * Animates a roll-pitch-yaw rotation about the body-fixed
 * coordinate frame
 */
void ZYXRelativePlot(RvizPlotter &plotter,const Eigen::Vector3d &rpy);

#endif