#include <Eigen/Core>
#include <me530646_lab1/lab1.h>
#include <ur5/utilities.h>
#include <math.h>
#include <iostream>

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the x axis (roll) of phi radians.
 */
Eigen::Matrix3d rollr(double phi){
	Eigen::Matrix3d r;
	r << 1,        0,         0,
		 0,	cos(phi), -sin(phi),
		 0, sin(phi),  cos(phi); 

	return r;
}

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the y axis (pitch) of theta radians.
 */
Eigen::Matrix3d pitchr(double theta){
	Eigen::Matrix3d r;
	r <<  cos(theta), 0,  sin(theta),
		           0, 1,           0,
		 -sin(theta), 0,  cos(theta); 

	return r;		 
}

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the z axis (yaw) of psi radians.
 */
Eigen::Matrix3d yawr(double psi){
	Eigen::Matrix3d r;
	r <<  cos(psi), -sin(psi), 0,
		  sin(psi),  cos(psi), 0,
		          0,        0, 1;

	return r;
}

/**
 * Returns the 3x3 rotation matrix that represents successive 
 * roll, pitch, and yaw rotations.
 */
Eigen::Matrix3d rpyr(const Eigen::Vector3d &rpy){
	Eigen::Matrix3d r = yawr(rpy(2))*pitchr(rpy(1))*rollr(rpy(0));
	return r;
}

/**
 * Returns the roll, pitch, and yaw from the given 3x3 rotation matrix.
 */
Eigen::Vector3d rpyrinv(const Eigen::Matrix3d &r){
	Eigen::Vector3d rpy;
	rpy(2) = atan2(r(1,0),r(0,0));
	rpy(1) = atan2(-r(2,0),sqrt(pow(r(0,0),2)+pow(r(1,0),2)));
	rpy(0) = atan2(r(2,1),r(2,2));
	return rpy;
}

/**
 * Returns the 4x4 homogeneous transformation that represents a 
 * translation of (x,y,z) and a rotation of (roll,pitch,yaw).
 */
Eigen::Matrix4d xf(const Eigen::VectorXd &xyzrpy){
	Eigen::Matrix4d h = Eigen::MatrixXd::Identity(4,4);
	h.block<3,3>(0,0) = rpyr(xyzrpy.block<3,1>(3,0));
	h.block<3,1>(0,3) = xyzrpy.block<3,1>(0,0);
	return h;
}

/**
 * Returns the matrix inverse to the given homogeneous transformation.
 */
Eigen::Matrix4d finv(const Eigen::Matrix4d &f){
	Eigen::Matrix3d r = f.block<3,3>(0,0);
	Eigen::Matrix3d rT = r.transpose();
	Eigen::Matrix4d hinv = Eigen::MatrixXd::Identity(4,4);;
	hinv.block<3,3>(0,0) = rT;
	hinv.block<3,1>(0,3) = -rT*f.block<3,1>(0,3);
	return hinv;
}

/**
 * Returns the homogenous transformation for rotation
 * about the x axis.
 */
Eigen::Matrix4d roll4(double roll)
{
	Eigen::Matrix4d r = Eigen::MatrixXd::Identity(4,4);
	r.block<3,3>(0,0) = rollr(roll);
	return r;
}

/**
 * Returns the homogenous transformation for rotation
 * about the y axis.
 */
Eigen::Matrix4d pitch4(double pitch)
{
	Eigen::Matrix4d r = Eigen::MatrixXd::Identity(4,4);
	r.block<3,3>(0,0) = pitchr(pitch);
	return r;
}

/**
 * Returns the homogenous transformation for rotation
 * about the z axis.
 */
Eigen::Matrix4d yaw4(double yaw)
{
	Eigen::Matrix4d r = Eigen::MatrixXd::Identity(4,4);
	r.block<3,3>(0,0) = yawr(yaw);
	return r;
}

/**
 * Animates a roll-pitch-yaw rotation about the world
 * coordinate frame
 */
void XYZFixedPlot(RvizPlotter &plotter,const Eigen::Vector3d &rpy)
{
	//Rotate in increments of 1 degree
	double inc = 1*(3.14159/180);
	Eigen::Matrix4d h = Eigen::MatrixXd::Identity(4,4);
	plotter.plotf(h,"XYZ");
	for(int i = 1; i <= rpy(0)/inc; i++)
	{
		plotter.plotf(roll4(i*inc)*h,"XYZ");
	}
	plotter.plotf(roll4(rpy(0))*h,"XYZ");
	for(int i = 1; i <= rpy(1)/inc; i++)
	{
		plotter.plotf(pitch4(i*inc)*roll4(rpy(0))*h,"XYZ");
	}
	plotter.plotf(pitch4(rpy(1))*roll4(rpy(0))*h,"XYZ");
	for(int i = 1; i <= rpy(2)/inc; i++)
	{
		plotter.plotf(yaw4(i*inc)*pitch4(rpy(1))*roll4(rpy(0))*h,"XYZ");
	}
	Eigen::Matrix4d f = yaw4(rpy(2))*pitch4(rpy(1))*roll4(rpy(0))*h;
	plotter.plotf(f,"XYZ");
} 

/**
 * Animates a yaw-pitch-roll rotation about the body-fixed
 * coordinate frame
 */
void ZYXRelativePlot(RvizPlotter &plotter, const Eigen::Vector3d &rpy)
{	
	//Rotate in increments of 1 degree
	double inc = 1*(3.14159/180);
	Eigen::Matrix4d h = Eigen::MatrixXd::Identity(4,4);
	plotter.plotf(h,"ZYX");
	for(int i = 1; i <= rpy(2)/inc; i++)
	{
		plotter.plotf(yaw4(i*inc)*h,"ZYX");
	}
	plotter.plotf(yaw4(rpy(2))*h,"ZYX");
	for(int i = 1; i <= rpy(1)/inc; i++)
	{
		plotter.plotf(yaw4(rpy(2))*pitch4(i*inc)*h,"ZYX");
	}
	plotter.plotf(yaw4(rpy(2))*pitch4(rpy(1))*h,"ZYX");
	for(int i = 1; i <= rpy(0)/inc; i++)
	{
		plotter.plotf(yaw4(rpy(2))*pitch4(rpy(1))*roll4(i*inc)*h,"ZYX");
	}
	plotter.plotf(yaw4(rpy(2))*pitch4(rpy(1))*roll4(rpy(0))*h,"ZYX");
} 