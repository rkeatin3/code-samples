#include <Eigen/Core>
#include <me530646_lab1/lab1.h>
#include <ur5/utilities.h>
#include <math.h>
#include <iostream>

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the x axis (roll) of phi radians.
 */
Eigen::Matrix3d rollr(double phi){
	//TODO
}

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the y axis (pitch) of theta radians.
 */
Eigen::Matrix3d pitchr(double theta){
	//TODO		 
}

/**
 * Returns the 3x3 rotation matrix that represents a rotation about 
 * the z axis (yaw) of psi radians.
 */
Eigen::Matrix3d yawr(double psi){
	//TODO
}

/**
 * Returns the 3x3 rotation matrix that represents successive 
 * roll, pitch, and yaw rotations.
 */
Eigen::Matrix3d rpyr(const Eigen::Vector3d &rpy){
	//TODO
}

/**
 * Returns the roll, pitch, and yaw from the given 3x3 rotation matrix.
 */
Eigen::Vector3d rpyrinv(const Eigen::Matrix3d &r){
	//TODO
}

/**
 * Returns the 4x4 homogeneous transformation that represents a 
 * translation of (x,y,z) and a rotation of (roll,pitch,yaw).
 */
Eigen::Matrix4d xf(const Eigen::Vector3d &xyzrpy){
	//TODO
}

/**
 * Returns the matrix inverse to the given homogeneous transformation.
 */
Eigen::Matrix4d finv(Eigen::Matrix4d f){
	//TODO
}

/**
 * Animates a roll-pitch-yaw rotation about the world
 * coordinate frame
 */
void XYZFixedPlot(RvizPlotter &plotter,const Eigen::Vector3d &rpy){
	//TODO

	//Hint:First apply the roll rotation in increments,
	//using plotf to draw the frame in rviz
}

/**
 * Animates a roll-pitch-yaw rotation about the body-fixed
 * coordinate frame
 */
void ZYXRelativePlot(RvizPlotter &plotter, const Eigen::Vector3d &rpy){
	//TODO
}


////////////////////Helpful Functions///////////////////
/**
 * Returns the homogenous transformation for rotation
 * about the x axis.
 */
Eigen::Matrix4d roll4(double roll)
{
	Eigen::Matrix4d r = Eigen::MatrixXd::Identity(4,4);
	r.block<3,3>(0,0) = rollr(roll);
	return r;
}

/**
 * Returns the homogenous transformation for rotation
 * about the y axis.
 */
Eigen::Matrix4d pitch4(double pitch)
{
	Eigen::Matrix4d r = Eigen::MatrixXd::Identity(4,4);
	r.block<3,3>(0,0) = pitchr(pitch);
	return r;
}

/**
 * Returns the homogenous transformation for rotation
 * about the z axis.
 */
Eigen::Matrix4d yaw4(double yaw)
{
	Eigen::Matrix4d r = Eigen::MatrixXd::Identity(4,4);
	r.block<3,3>(0,0) = yawr(yaw);
	return r;
}
