#include <Eigen/Core>
#include <ur5/utilities.h>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <me530646_lab3/lab3.h>
#include <me530646_lab4/lab4.h>

#include <stdlib.h>
#define PI M_PI

int main(int argc, char **argv){
	ros::init(argc,argv,"lab3");
	ros::NodeHandle n;
	UR5 robot = UR5(n);

	//How to calculate the trace of a matrix
	Eigen::MatrixXd M= Eigen::MatrixXd::Random(4,4);
	double trace = M.trace();

	Eigen::VectorXd q(6);
	Eigen::MatrixXd sol;
	int num; 
	q << 1.5*PI,1.5*PI,2*PI/3,7*PI/6,PI/3,5*PI/3;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 8)
	{
		printf("8.1: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("8.1:Solution incorrect\n");
	}
	q << 2*PI/3,2*PI,5*PI/6,5*PI/3,5*PI/6,PI;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 8)
	{
		printf("8.2: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("8.2:Solution incorrect\n");
	}


	q << PI,2*PI,2*PI,PI,0,0;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 6)
	{
		printf("6.1: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("6.1:Solution incorrect\n");
	}
	q << 7*PI/6,1.5*PI,PI/6,2*PI/3,7*PI/6,5*PI/3;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 6)
	{
		printf("6.2: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("6.2:Solution incorrect\n");
	}


	q << 0,0,PI/6,0,0,2*PI/3;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 4)
	{
		printf("4.1: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("4.1:Solution incorrect\n");
	}
	q << 5*PI/3,2*PI/3,5*PI/3,2*PI,5*PI/3,PI;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 4)
	{
		printf("4.2: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("4.2:Solution incorrect\n");
	}


	q << PI/2,5*PI/6,11*PI/6,2*PI,PI,11*PI/6;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 2)
	{
		printf("2.1: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("2.1:Solution incorrect\n");
	}
	q << 7*PI/6,PI/3,11*PI/6,1.5*PI,5*PI/6,PI/6;
	num = inverse(UR5::fwd(q),sol);
	if(num/2 != 2)
	{
		printf("2.2: %d solutions\n",num/2);
	}
	for(int i = 0; i < num/2; i++)
	{
		if(!matrixEquals(UR5::fwd(q),UR5::fwd(sol.block<6,1>(0,i))))
			printf("2.2:Solution incorrect\n");
	}

	Eigen::VectorXd xyzrpy(6);
	xyzrpy << 5,5,5,0,0,0;
	Eigen::MatrixXd test1 = xf(xyzrpy);
	num = inverse(test1,sol);
	if(num != 0)
	{
		printf("Doesn't check inside sphere of workspace\n");
	}
	xyzrpy << 0,0,0,0,0,0;
	Eigen::Matrix4d test2 = xf(xyzrpy);
	num = inverse(test2,sol);
		if(num != 0)
	{
		printf("Doesn't check outside empty cylinder of workspace\n");
	}


	Eigen::MatrixXd Jac(6,6);
	q << PI/2,5*PI/6,11*PI/6,2*PI,PI,11*PI/6;
	Jac << -0.646155, -8.65101e-18, 8.74346e-19, -7.91699e-18, -9.04631e-19, 0,  0.02685, 0.504873, 0.292373, -0.047325, -0.04115, 8.72853e-18,  0, 0.646155, 0.278094, 0.0819693, 0.0712739, 1.88074e-17,  0, 1, 1, 1, 8.36449e-17, -1,  0, -6.12323e-17, -6.12323e-17, -6.12323e-17, 0.866025, 2.28522e-16,  1, 6.12323e-17, 6.12323e-17, 6.12323e-17, 0.5, -1.06058e-16;
	if(!matrixEquals(J(q),Jac))
		printf("Fails first Jacobian test\n");

	q << PI,2*PI,2*PI,PI,0,0;
	Jac << -0.19145, 0.09465, 0.09465, 0.09465, -0.0823, -5.03942e-18,  0.81725, 3.84508e-17, 1.24271e-17, -1.15913e-17, -5.30659e-17, 0,  0, -0.81725, -0.39225, 1.34468e-16, -5.03942e-17, 1.00788e-17,  0, 1.22465e-16, 1.22465e-16, 1.22465e-16, -6.12323e-16, 1.22465e-16,  0, 1, 1, 1, 6.16298e-32, 1,  1, 6.12323e-17, 6.12323e-17, 6.12323e-17, 1, 6.12323e-17;
	if(!matrixEquals(J(q),Jac))
		printf("Fails second Jacobian test\n");

	q << 2*PI/3,2*PI,5*PI/6,5*PI/3,5*PI/6,PI;
	Jac << -0.0270341, -0.118637, -0.118638, -0.020575, -0.0356369, 2.42251e-18,  0.0281274, 0.205486, 0.205486, 0.0356369, -0.020575, -4.5689e-18,  0, 0.00934846, 0.434348, 0.09465, 0.0712739, 4.87891e-19,  0, 0.866025, 0.866025, 0.866025, -0.5, -0.75,  0, 0.5, 0.5, 0.5, 0.866025, -0.433013,  1, 6.12323e-17, 6.12323e-17, 6.12323e-17, -1.48807e-17, -0.5;
	if(!matrixEquals(J(q),Jac))
		printf("Fails third Jacobian test\n");

}




