#include <Eigen/Core>
#include <ur5/utilities.h>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <me530646_lab3/lab3.h>
#include <me530646_lab4/lab4.h>

int main(int argc, char **argv){
	ros::init(argc,argv,"lab3");
	ros::NodeHandle n;
	UR5 robot = UR5(n);

	//How to calculate the trace of a matrix
	Eigen::MatrixXd M= Eigen::MatrixXd::Random(4,4);
	double trace = M.trace();

	//How to call inverse
	Eigen::VectorXd q1(6); 
	q1 << 3.2,2.5,0,0.5,1.3,0.2;
	Eigen::MatrixXd q_sol;
	int num_sol1 = inverse(UR5::fwd(q1),q_sol);
	printEigen(q_sol);
	for(int i = 0; i < num_sol1; i++)
	{
		printEigen(q_sol.block<6,1>(0,i));
		robot.plotfwd(q_sol.block<6,1>(0,i));
		sleep(1);
	}
  }


