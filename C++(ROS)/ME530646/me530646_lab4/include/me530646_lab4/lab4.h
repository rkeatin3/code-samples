#ifndef LAB4_H
#define LAB4_H

#include <Eigen/Core>
#include <ur5/utilities.h>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <me530646_lab3/lab3.h>

int inverse(const Eigen::Matrix4d &H0_6, Eigen::MatrixXd &q, double q6Des = 0.0);

Eigen::MatrixXd J(const Eigen::VectorXd &q);

#endif
