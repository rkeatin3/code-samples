\documentclass[10pt, letterpaper]{article}

\input{../hdr530646}

\graphicspath{{images/}}


\title{M.E. 530.646 Project (REV 1)}
\date{}

\author{Ryan Keating and Noah Cowan\\Johns Hopkins University}

\newcommand{\Real}{\mathbb{R}}
\newcommand{\SE}[1]{\mathrm{SE}(#1)}
\newcommand{\SO}[1]{\mathrm{SO}(#1)}
\begin{document}

\maketitle

\setlength{\parindent}{0pt}

Put the ROS package for project in the \texttt{src} directory of your catkin workspace. \\

In this project, you will put to use what you have learned about robot kinematics this semester. You will compare trajectories planned in Cartesian space with Jacobian inverse and transpose methods to trajectories planned in joint space.\\

To complete this assignment, your team will write code in
\texttt{project\_main.cpp}. You will use rviz to test your code before running it on the actual UR5 hardware. To start
rviz with the the UR5 model imported, enter the following command in the
terminal: 
\\ \fbox{\texttt{roslaunch ur5 ur5\_rviz.launch}}

\section*{Assignment}
\textbf{Be sure to comment all of your code!}\\

In your report, show some examples to show each step works! Also, the
instructor may test certain standard configurations above and beyond
your examples.
\begin{enumerate}
\item Denote $T = T_6^0$ (to avoid proliferation of sub- and
  superscripts). Let $T = (R,p)\in\SE{3}$, where $R\in\SO{3}$ is the
  rotational part and $p\in\Real^3$ is the translational part.
  Suppose you are given \emph{any} two nontrivial homogenous
  transformations defining the position of the end-effector of the UR5
  with respect to its base. These will be denoted as
  $T_i=(R_i,p_i),T_f=(R_f,p_f)\in\SE{3}$ for the initial and final
  configurations, respectively. 

\item Write code to perform a test to ensure that these are both
  within the workspace of the robot. 

  % Ryan: One possibility is to try to compute inverse kinematics for
  % each; if there is no solution that may be a clue!)

\item Perform a test for whether or not either of the configurations
  is singular. 

\item Execute a trajectory in joint space from $T_i$ to $T_f$:
  \begin{enumerate}
  \item Using the inverse kinematics solutions for each of the two
    transformation (initial pose and final pose), choose the pair such
    that the joint space distance between them is minimized
    (i.e. minimize $||\vec{q_F}-\vec{q_I}||_2$)
  \item Linearly interpolate between the initial and final joint
    values to define the trajectory.
  \end{enumerate}

\item Execute two trajectories in Cartesian space from $T_i$ to
  $T_f$. In each case, you need to compute a velocity vector that
  ``points'' from the current configuration to the final
  configuraiton. 

  One way to do this is to compute the ``straight line'' between the two
  poses. Since SO(3) is a very curvy space, there is no perfect
  ``straight line'' but one nice way to do this is to calculate the
  ``error'' between the two poses:
  \begin{equation}
    \label{eq:1}
    \tilde{R}  = R R_f^T 
  \end{equation}
  where $R$ is the \emph{current} pose (not simply the initial pose,
  although obviously at $t=0$, $R=R_i$). The special thing about the
  error rotation $\tilde{R}$ is it can be written as the exponential
  of some skew symmetric matrix $\hat{\mathbf{w}}$ (corrected),
  \begin{equation}
    \label{eq:2}
    e^{\hat{\mathbf{w}}} =  \tilde{R}
  \end{equation}
  where the vector $\mathbf{w}\in\Real^3$ is the closest thing to a
  straight line distance! 

  So, we take
  \begin{equation}
    \label{eq:3}
    \omega = - k_\omega \mathbf{w}
  \end{equation}
  where $\omega$ is the body velocity, and
  \begin{equation}
    \label{eq:4}
    v = - k_v (p - p_f)
  \end{equation}
  where the gains $k_\omega$ and $k_v$ define the speed of
  convergence. 

  \begin{enumerate}
  \item Prove that this works, namely that, assuming the body can
    always follow the $\omega$ and $v$ above, that this will
    eventually arrive at the end goal. This question does NOT involve
    anything to do with the kinematics of the UR5! It is as if there
    are little jet packs on the end effector, driving it along the
    desired $\omega$ and $v$. 

    \emph{Hint:} The translational part of this is fairly easy, so try
    that first (treat it as just moving a point in 3D space).

    \emph{Hint:} Do this in simulation to convince yourself it is
    right! It is POSSIBLE Prof.~Cowan made a mistake in his
    formulation. 

    
  \item Use the transpose of the Jacobian to map the angular and
    linear velocities into the generalized coordinates:
    \begin{equation}
      \label{eq:5}
      \dot{q}* = J^T
      \begin{bmatrix}
        v \\ 
        \omega
      \end{bmatrix}
    \end{equation}
    This gives a desired joint velocity. Since we are going to
    implement ``small steps'' in joint space, you'll need to find a
    (small) discrete motion based on this desired joint velocity.

    Execute the trajectory such that $q_{k+1} = q_k + \epsilon
    \dot{q}^*$, where $\epsilon$ is a small number. You'll have to
    choose this parameter to be ``small enough'' to give a good
    approximation to the derivative and ``large enough'' that the
    robot doesn't move too slowly. 
  \item Repeat the previous problem, but instead of using $J^T$ as the
    mapping into joint space, use $J^{-1}$. Make sure to use your code
    that checks being too close to a singularity, and abort the
    experiment and give an error to the user if this happens!
  \end{enumerate}

\item Compare the three trajectories executed in the previous
  questions.

\item Expand on these techniques in a creative way to
  demonstrate what you've learned.\\

  Suggestions:
  \begin{itemize}
  \item Use the under-actuated gripper on the UR5 to do pick-and-place
    of some objects.
  \item Plan a trajectory with path constraints (e.g. workspace
    obstacles).
  \item Find a way to deal with passing through or very near to
    singularities, when using the inverse / transpose Jacobian
    matrix. In the inverse case, the calculation of the inverse can
    ``blow up'', and in the transpose case, you can get stuck in the
    null space of the transpose Jacobian and never get to your goal.
  \item Implement a vision-based control system. See Prof.~Cowan for
    references if you are interested in this!
  \item Use a Kinect and do something cool.
  \end{itemize}
\end{enumerate}
\end{document}
