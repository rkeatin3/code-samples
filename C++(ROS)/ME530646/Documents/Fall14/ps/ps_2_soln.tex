\documentclass[10pt]{article} 

\usepackage{graphicx,amsmath,amsfonts,amssymb,enumerate,cancel}
\def\p{\vec{p}}
\def\q{\vec{q}}
\def\r{\vec{r}}
\def\s{\vec{s}}
\def\x{\vec{x}}
\def\w{{\omega}} 

\DeclareMathOperator{\Tr}{Tr}

\textwidth = 6.5 in 
\textheight = 9 in 
\oddsidemargin = 0.0 in
\evensidemargin = 0.0 in 
\topmargin = 0.0 in 
\headheight = 0.0 in
\headsep = 0.0 in

\input{cowanhdr}

\title{
  {\normalsize ME 530.646\\ Introduction to Robotics} \\
  \vspace{0.25in} Homework 2 Solutions}
\author{Noah J. Cowan} 
\date{Fall 2014}
\begin{document}
\maketitle

\begin{enumerate}
\item To derive Rodrigues formula for $x\in\mathbb{R}^3$, $\|x\|=1$, and $\theta\in\mathbb{R}$, consider the Taylor expansion
\begin{equation}
  e^{\hat x \theta} = I + \hat x \theta + \frac{1}{2} (\hat x)^2\theta^2 + \frac{1}{3!} (\hat x)^3\theta^3 + \dotsc
\end{equation}
Note that the powers of $\hat x$ cycle
\begin{align*}
  (\hat x)^{2n} &= (-1)^n(\hat x)^2 \\
  (\hat x)^{2n+1} &= (-1)^n\hat x
\end{align*}
for $n\mathbb{Z}^{\geq 0}$. Two ways to prove this are induction or the Cayley--Hamilton theorem. Showing that $(\hat x)^4 = -(\hat x)^2$ is not sufficient to establish this cycling. Next, recall the Taylor expansion of $\sin$ and $\cos$
\begin{align*}
  \sin(x) &= x - \frac{1}{3!}x^3 + \frac{1}{5!}x^5 + \dotsc \\
  \cos(x) &= 1 - \frac{1}{2}x^2 + \frac{1}{4}x^4 - \dotsc
\end{align*}
Grouping terms and combining, we see that
\begin{equation}
  e^{\hat x \theta} = I + \sin\theta\hat x + (1-cos\theta)\hat x ^ 2.
\end{equation}

\item Let $R\in\text{SO}(3)$ be some rotation matrix that can be rewritten using Rodrigues formula as
\begin{equation}\label{eq:2:r}
  R = I + \sin\theta\hat x + (1-\cos\theta)\hat x^2.
\end{equation}
Recall that any square matrix $M$ can be written as the sum of a symmetric and skew-symmetric matrix:
\begin{equation*}
  M = \frac{1}{2}(M + M^\intercal) + \frac{1}{2}(M - M^\intercal).
\end{equation*}
Using this identity, we can write the following set of equations equating the skew-symmetric and symmetric matrices.
\begin{align}
  \sin\theta\hat x &= \frac{1}{2}(R - R^\intercal)\label{eq:2:skew} \\
  I + (1-\cos\theta)\hat x^2 &= \frac{1}{2}(R + R^\intercal)\label{eq:2:sym}
\end{align}
First consider (\ref{eq:2:skew}). As a skew-symmetric matrix, we know there are only three independent quantities; these fully define the axis of rotation $x$. For $R = [r_{ij}]$,
\begin{equation}
\boxed{
  x = \frac{1}{2\sin\theta}\begin{bmatrix}
    r_{32} - r_{23} \\
    r_{13} - r_{31} \\
    r_{21} - r_{12}
    \end{bmatrix}}
\end{equation}
Note that if $\sin\theta=0$ (i.e., $\theta\in\{0,\pi\}$), $x$ can be an arbitrary unit vector. (Rotations of 0 or $\pi$ degrees give the same result about any axis.) 

Next, consider (\ref{eq:2:sym}), the symmetric equation. Note that $\Tr(\hat x^2) = -2\|x\| = -2$ since $\x\|=1$., Therefore,
\begin{align*}
  \Tr(\frac{1}{2}(R+R^\intercal)) &= \Tr(I + (1-\cos\theta)\hat x^2) \\
   &= \Tr(I) + \Tr(1-\cos\theta)\hat x^2) \\
   &= 3 + (1-\cos\theta)(-2) \\
   &= 1 + 2\cos\theta \\
   \Tr(R) &= 1+2\cos\theta
\end{align*}
Solve for $\theta$ shows that
\begin{equation}
\boxed{
  \theta = \arccos\frac{\Tr(R) - 1}{2}
}
\end{equation}

\item
\begin{enumerate}[(a)]
  \item In general, matrices of $\mathbb{R}^{2x2}$ have four degrees of freedom and those of $\mathbb{R}^{3x3}$ have nine degrees of freedom (i.e., the number of elements).
  \item
    \begin{enumerate}[i{.}]
      \item $\mathfrak{so}(2)=\{A\in\mathbb{R}^{2x2} | A=-A^\intercal$
      \item One degree of freedom.
      \item It is skew-symmetric, therefore zeros on the diagonals and the remaining two entries must be negatives of each other.
      \item $\forall\alpha\in\mathbb{R},\; A = \alpha\begin{bmatrix}
        0 & 1 \\
        -1 & 0
        \end{bmatrix} $
     \end{enumerate}
  \item
    \begin{enumerate}[i{.}]
      \item $\text{SO}(2)=\{A\in\mathbb{R}^{2x2} | AA^\intercal = I, \det A = 1$
      \item One degree of freedom.
      \item $A$ must be an orthogonal matrix, which happens to be enough to make this one degree of freedom. (Orthogonal matrices can have determinants of $\pm1$, the restriction of the $\det A = 1$ restricts this set further, but does not change the degrees of freedom.)
      \item $\forall\alpha\in\mathbb{R},\; A = \exp\left(\alpha\begin{bmatrix}
        0 & 1 \\
        -1 & 0
        \end{bmatrix}\right)$
     \end{enumerate}
  \item
    \begin{enumerate}[i{.}]
      \item $\mathfrak{so}(3)=\{A\in\mathbb{R}^{3x3} | A=-A^\intercal$
      \item Three degrees of freedom.
      \item It is skew-symmetric, therefore zeros on the diagonals and the remaining six entries must be pairs of positive/negative values.
      \item $\forall x,y,z\in\mathbb{R},\; A = \begin{bmatrix}
        0 & -z & y \\
        z & 0 & -x \\
        -y & x & 0
        \end{bmatrix} $
     \end{enumerate}
  \item
    \begin{enumerate}[i{.}]
      \item $\text{SO}(3)=\{A\in\mathbb{R}^{3x3} | AA^\intercal = I, \det A = 1$
      \item Three degrees of freedom.
      \item $A$ must be an orthogonal matrix, which happens to be enough to make this three degrees of freedom. (Orthogonal matrices can have determinants of $\pm1$, the restriction of the $\det A = 1$ restricts this set further, but does not change the degrees of freedom.)
      \item $\forall x,y,z\in\mathbb{R},\; A = \exp\left(\begin{bmatrix}
        0 & -z & y \\
        z & 0 & -x \\
        -y & x & 0
        \end{bmatrix}\right)$
     \end{enumerate}
\end{enumerate}
\item 
\begin{enumerate}[(a)]
  \item With the right choice of coordinate frame, this problem is straight-forward. Choose the origin to be at the base of the first link, x-axis point right, y-axis pointing up.
  \item $\theta_1$ is from the positive x-axis to the bar of length $L_1$. Positive rotation is counter-clockwise about the z-axis (per the right hand rule). $\theta_2$ is defined from the first link to the second link. Again, positive rotation is counter-clockwise about the z-axis. See Figure 1.22 (p. 23) in Spong for a good visual.
  \item The link kinematic parameters are $L_1$ and $L_2$.
  \item The forward kinematics for position, $x_{ee}$ are given by simple geometry
    \begin{equation*}
      \boxed{
      x_{ee} = \begin{bmatrix}
        L_1\cos\theta_1 + L_2\cos(\theta_1 + \theta_2) \\
        L_1\sin\theta_1 + L_2\sin(\theta_1 + \theta_2)
      \end{bmatrix}}
    \end{equation*}
  \item The robot's gripper cannot reach all the points in $\mathbb{R}^2$ because the links are inextensible (i.e., finite in length).
  \item The position of the end-effector cannot be further from the origin than $L_1 + L_2$ as the links are not extensible. Neglecting the wall, it cannot be closer to the origin than $|L_1-L_2|$. Thus
\begin{equation}
  \boxed{
    |L_1-L_2| \leq \|x_{ee}\| \leq L_1 + L_2
}
\end{equation}. If we consider the wall, it must also be the case that $x_{ee}(2) \geq 0$, i.e., the y-coordinate of the end-effector is nonnegative.
  \item The inverse kinematics for this manipulator are not unique, there is an \textit{elbow up} and \textit{elbow down} configuration. See p. 23 in Spong for deriving 
\begin{equation*}
  \boxed{
    \theta_2 = \arctan\frac{\pm\sqrt{1-D^2}}D
}
\end{equation*} for $D=\frac{\|x_{ee}\| - L_1^2-L_2^2}{2L_1L_2}$. To solve for $\theta_1$, consider the line between the origin and the end-effector of length $\|x_{ee}\|$. This is at an angle $\alpha$ with the x-axis. Notably, $\tan\alpha = \frac{y}{x}$. Next, solve the for the angle between the first link and the line to the end-effector, $\beta = \alpha - \theta_1$. From the geometry of the problem, $\tan\beta = \frac{L_2\sin\theta_2}{L_1 + L_2\cos\theta_2}$. Then,
\begin{equation*}
  \boxed{
    \theta_1 = \arctan\frac{y}{x} - \arctan\frac{L_2\sin\theta_2}{L_1+L_2\cos\theta_2}
}
\end{equation*}
  \item
    \begin{align*}
      \dot x &= \begin{bmatrix}
        -L_1\dot\theta_1\sin\theta_1 - L_2(\dot\theta_1+\dot\theta_2)\sin(\theta_1 + \theta_2) \\
        L_1\dot\theta_1\cos\theta_1 + L_2(\dot\theta_1+\dot\theta_2)\cos(\theta_1 + \theta_2)
      \end{bmatrix} \\    
       &= \begin{bmatrix}
        -L_1\sin\theta_1-L_2\sin(\theta_1 + \theta_2) & -L_2\sin(\theta_1+\theta_2) \\
        L_1\cos\theta_1 + L_2\cos(\theta_1+\theta_2) & l_2\cos(\theta_1+\theta_2)
        \end{bmatrix} \begin{bmatrix}
        \dot\theta_1 \\
        \dot\theta_2
        \end{bmatrix} \\
      &= J\dot q
      \end{align*}
\end{enumerate}

\item Let $g_1,g_2,g_3\in\SO{3} \times \Real^3$  where $g_i=(R_i,x_i)$, $i=1,2,3$.
	\begin{enumerate}[i{.}]
		\item Closure:  $g_1 \circ g_2 = (R_1 R_2, x_2 + x_1)$, and note that  $R_1 R_2 \in \SO{3}$ and $ x_2 + x_1 \in\Real^3$, and thus $g_1  \circ  g_2\in\SE{3}$.
		\item Associativity.  By direct computation,
			\[
        				g_1 \circ (g_2 \circ  g_3) = (R_1 R_2 R_3,  x_1 + x_2 + x_3) = (g_1 \circ  g_2) \circ  g_3.
			\] 
		\item Identity is given by $e=(I,0)$ since $g \circ e = (R I, x+0) = (R,x) =  g$ and $e \circ g = (I R, 0+x) = (R,x)=g$, for all $g=(R,x)\in\SO{3} \times \Real^3$.
		\item Inverse: If $g=(R,x)\in\SO{3} \times \Real^3$, then $g^{-1} = (R^T, -x)$ (easy to check that $g  g^{-1} = g^{-1} g = e$).  
			Note, that $R^T\in\SO{3}$ and $  -x \in\Real^3$, and thus $g^{-1}\in\SO{3} \times \Real^3$.
	\end{enumerate}
\item Let $g_1,g_2,g_3\in\SE{3}$  where $g_i=(R_i,x_i)$, $i=1,2,3$.
\begin{enumerate}[a{.}]
\item Prove \SE{3} is a group. The usual...
\begin{enumerate}[i{.}]
\item Closure.  $g_1 \circ g_2 = (R_1 R_2, R_1 x_2 + x_1)$, and note that
  $R_1 R_2 \in \SO{3}$ and $R_1 x_2 + x_1 \in\Real^3$, and thus $g_1  \circ
  g_2\in\SE{3}$.
\item Associativity.  By direct computation, 
\[
        g_1 \circ (g_2 \circ  g_3) = (R_1 R_2 R_3, R_1 R_2 x_3 + R_1 x_2 + x_1)
        = (g_1  \circ  g_2) \circ   g_3.
\]
\item Identity is given by $e=(I,0)$ since $g\circ  e = (R I, x+0) = (R,x) =
  g$ and $e \circ g = (I R, 0+x) = (R,x)=g$, for all $g=(R,x)\in\SE{3}$.
\item Inverse: If $g=(R,x)\in\SE{3}$, then $g^{-1} = (R^T, -R^T x)$ (easy to
  check that $g \circ g^{-1} = g^{-1}\circ g = e$).  Note, that $R^T\in\SO{3}$
  and $ -R^T x \in\Real^3$, and thus $g^{-1}\in\SE{3}$.
\end{enumerate}
\item Prove \SE{3} has a well defined action on points in $\Real^3$
  given by $g(p) = R p + v$, where $g=(R,v)\in\SE{3}$ and
  $p\in\Real^3$.
\begin{enumerate}[i{.}]
\item Closure: $g(p) = R p + v \in\Real^3$.
\item Identity:  $e(p) = I p + 0 = p$.
\item Associativity:  $g_1 (g_2 (p)) = R_1 R_2 p + R_1 x_2 + x_1 = (g_1 \circ  g_2)(p)$. (direct computation)
\end{enumerate}


\item Prove that the induced action on vectors, $g_*(v):=g(v+p)-g(v)$
  is given by $g_*(v)= R v$. 

	{\em  $g_*(v):=g(v+p)-g(v) \Rightarrow g_*(v)= R v$ was done in HW 1, problem 4b.}
 
 To show  $ g_*(v)= R v$ is a well defined action:
 \begin{enumerate}[i{.}]
	\item Closure: $g_*(v) = Rv \in \Real^3$
	\item Identity: $e_*(v) = Iv = v$.
	\item Associativity:  $g_{1*} (g_{2*} (v)) = g_{1*} (R_2v_1)  = R_1(R_2v_1)=( R_1 R_2) v _1 =(g_{1*} \circ  g_{2*})(v)$. (by associativity of matrix multiplication)
\end{enumerate}

\item Prove \SE{3} is a group by using the homogeneous representation.
  (Admittedly, this is getting redundant, but hopefully everyone
  really knows what a group is by the end of all this!)  Let
  $g_1,g_2,g_3\in\SE{3}$ represented in matrix form as
\[
        g_1=\begin{bmatrix} R_1 & x_1 \\ 0^T & 1 \end{bmatrix},\quad
        g_2=\begin{bmatrix} R_2 & x_2 \\ 0^T & 1 \end{bmatrix},\quad
        g_3=\begin{bmatrix} R_3 & x_3 \\ 0^T & 1 \end{bmatrix}
\]
such that $R_i\in\SO{3},x_i\in\Real^3, i=1,2,3$.
\begin{enumerate}[i{.}]
\item Closure.  
\[
        g_1 \circ  g_2 = 
        \begin{bmatrix} R_1 R_2 & R_1 x_2 + x_1 \\ 0^T & 1 \end{bmatrix}
\]
and $R_1 R_2 \in \SO{3}$ and $R_1 x_2 + x_1 \in\Real^3$, and thus $g_1\circ  g_2\in\SE{3}$. 
\item Associativity is given by associativity of matrix multiplication.  

Alternatively, by direct computation, 
\[
        g_1 \circ (g_2 \circ g_3) = 
        \begin{bmatrix}
                R_1 R_2 R_3 &  R_1 R_2 x_3 + R_1 x_2 + x_1\\
                0^T & 1 
        \end{bmatrix}
        = (g_1 \circ g_2) \circ  g_3.
\]
\item Identity is given by the $4\times 4$ identity matrix, $I$, since
  $g \circ I = I \circ g = g$, for all $g\in\SE{3}$ (in fact, this is true for all
  $4\times4$ matrices.
\item If 
\[
        g=\begin{bmatrix} R & x \\ 0^T & 1 \end{bmatrix}
        \quad\text{then}\quad
        g^{-1}=\begin{bmatrix} R^T & -R^T x \\ 0^T & 1 \end{bmatrix}
\]
since $g \circ g^{-1} =g^{-1}\circ  g = I_{4 \times 4}$ (easy to check).
\end{enumerate}


\end{enumerate}

\item MLS p73, Exercise 3. Let $w\in\Real^3$, $\|w\|=1$,
  $R=\exp\{\hat{w}\theta\}$.
\begin{enumerate}[{(}a{)}]
\item Eigenvector equation: $\hat{w} v = \lambda v$ or, equivalently
  $(\hat{w} - I \lambda) v = 0$.  Thus, $v\in\ker{ (\hat{w} - I
    \lambda)}$.  In order for $(\hat{w} - I \lambda)$ to be singular
  (and, thus, have a kernel)
\[
        0 = |(\hat{w} - I \lambda)|=
        \begin{vmatrix} 
                -\lambda & -w_3 & w_2 \\
                w_3 & -\lambda & -w_1 \\
                -w_2 & w_1 & -\lambda 
        \end{vmatrix} = -\lambda(\lambda^2 + \|w\|^2)
        \iff
        \lambda = 0 \text{ or } \pm {i}
\]
Thus, the eigenvalues are $0, \pm {i}$. The eigenvectors are found by
solving $0 = (\hat{w} - I \lambda)v$.
\[
        \lambda = 0 \,\implies\, 0 = (\hat{w}) v = w\times v \quad \implies v = w
                         \text{ (up to a scale)}
\]
For the other two, note that given a singular $3\times3$ matrix, the
null space can be found by taking the cross product of two linearly
independent rows.  This is simply because the nullspace vector must be
simultaneously orthogonal to all rows, and the cross product of two
vectors is orthogonal to both vectors.  Thus:
\[
\lambda = +{i} \,\implies\, 0 = (\hat{w} - {i} I) v
=\overbrace{\begin{bmatrix}
    -{i} & -w_3 & w_2 \\
    w_3 & -{i} & -w_1 \\
    -w_2 & w_1 & -{i}
        \end{bmatrix}}^{M=\begin{bmatrix} m_1^T\\m_2^T\\m_3^T\end{bmatrix}}
\implies
        v = m_2\times m_3
        = \begin{bmatrix}
                        -1 + w_1^2 \\
                        w_1 w_2 + i w_3\\
                        w_1 w_3 - i w_2
                \end{bmatrix}
\]
Note that $v$ is nonzero for $\|w\|=1$, and thus $m_2$ and $m_3$ must be linearly independent for all $w$, as required.  Similarly, for every eigenvalue / eigenvector pair, the complex conjugate is also an eigenvalue / eigenvector pair, and thus
\[
        \lambda = -{i} \,\implies\, 
        v = \begin{bmatrix}
                        -1 + w_1^2 \\
                        w_1 w_2 - i w_3\\
                        w_1 w_3 + i w_2
                \end{bmatrix}
\]
\item 
{\em Method 1}: Use the eigenvalue equation (plug-and-chug, substituting $\|w\|^2=1$ and $\sin^2\theta + \cos^2\theta = 1$, and a TON of details omitted):
\[
        0=|R - \lambda I| = 
        |(1-\lambda)I + \sin\theta \hat{w}  + (1-\cos\theta)\hat{w}^2|
         = (1-\lambda)(\lambda^2 - 2\lambda \cos\theta +1)
\]
the roots of which are given by $\lambda = 1$, $\lambda=\cos\theta+ i\sin\theta=e^{i\theta}$ and $\lambda = \cos\theta - i\sin\theta = e^{-i\theta}$.

{\em Method 2:} Let $A\in\Real^{3\times 3}$ be a matrix, and let $V = [v_1,v_2,v_3]$ be the matrix of eigenvectors, and let $\Lambda=\diag{\lambda_1,\lambda_2,\lambda_3}$ be the matrix whose diagonal elements are the eigenvalues.  Then the eigenvector equations $A v_i = \lambda_i v_i$, $i=1,2,3$ is given by $A V = V \Lambda$.  In other words
\begin{equation*}
        V^{-1} A V = \Lambda \iff
        e^{V^{-1} A V} = e^{\Lambda} 
\end{equation*}
but note that $(V^{-1}  A V)^n = (V^{-1}  A V)(V^{-1}  A V)\cdots (V^{-1}  A V) = V^{-1}  A^n V$, and therefore
\begin{align*}
        e^{V^{-1} A V}
         &= (I + V^{-1} A V + \frac{1}{2!} (V^{-1} A V)^2 +
                \frac{1}{3!} (V^{-1} A V)^3 + \cdots)\\
        &=V^{-1} (I + A + \frac{1}{2!} A^2 +
                \frac{1}{3!} A^3 + \cdots)V^{-1} \\
        &= V^{-1} e^A V\\
\end{align*}
and, expanding the Taylor series of $e^{\Lambda}$, since it is diagonal, shows that it is just the exponential of the diagonal elements, and therefore 
\begin{align*}
         e^{\Lambda} = \diag{e^{\lambda_1},e^{\lambda_2},e^{\lambda_3}}
\end{align*}
So, we have that 
\[
        A V = V \Lambda  \iff
         e^{ A }V = V e^{\Lambda} 
                \quad\text{Note: this is just the eigenvector equation for }e^A
\]
and therefore the eigenvectors are unchanged, and the eigenvalues of $e^A$ are just $e^{\lambda_i}$.  So, if $A=\hat{w}$, which has eigenvalues $0,-i\theta,+i\theta$, then $e^{\hat{w}}$ has eigenvalues $e^0=1, e^{\pm i \theta}$.

\item  In fact, let $A=[a,b,c]$ be any $3\times3$ matrix with columns $a_i, i=1,2,3$.  Then
\[
        |A| = a \cdot (b\times c) 
\]
To see this, it is a bit tedious, but just write the elements of the matrix, and take the determinant by going down the first row:
\begin{align*}
        |A| &=
        \begin{vmatrix} 
                a_1 & b_1 & c_1\\
                a_2 & b_2 & c_2\\
                a_3 & b_3 & c_3\\
        \end{vmatrix}
        = a_1 \begin{vmatrix}
                        b_2 & c_2\\
                        b_3 & c_3
                \end{vmatrix}
        + a_2 \begin{vmatrix}
                        b_3 & c_3\\
                        b_1 & c_1
                \end{vmatrix}
        + a_3 \begin{vmatrix}
                        b_1 & c_1\\
                        b_2 & c_2
                \end{vmatrix}\\
        &= a_1 (b_2 c_3 - b_3 c_2) + a_2(b_3 c_1 - b_1 c_3)
                + a_3 (b_1 c_2 - b_2 c_1)\\
        &= a \cdot (b\times c)
\end{align*}
Direct computation also reveals that $|A| = a\cdot(b\times c) =
c\cdot(a\times b) = b\cdot(c\times a)$.  Note how you cycle through
the columns cyclically.  Of course for a rotation matrix,
$R=[r_1,r_2,r_3]$, we also have that
\[
        |R| = r_1 \cdot(r_2\times r_3) = r_1 \cdot (r_1) = 1
\]
since $r_2\times r_3 = r_1$ for a proper rotation.
\end{enumerate}



\end{enumerate}

\end{document}
