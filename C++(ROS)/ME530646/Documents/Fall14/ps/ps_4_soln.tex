\documentclass[10pt]{article} 

\usepackage{graphicx,amsmath,amsfonts,amssymb,enumerate,cancel}
\def\p{\vec{p}}
\def\q{\vec{q}}
\def\r{\vec{r}}
\def\s{\vec{s}}
\def\x{\vec{x}}
\def\w{{\omega}} 

\DeclareMathOperator{\Tr}{Tr}
%\DeclareMathOperator{\det}{det}

\textwidth = 6.5 in 
\textheight = 9 in 
\oddsidemargin = 0.0 in
\evensidemargin = 0.0 in 
\topmargin = 0.0 in 
\headheight = 0.0 in
\headsep = 0.0 in

\input{cowanhdr}

\title{
  {\normalsize ME 530.646\\ Introduction to Robotics} \\
  \vspace{0.25in} Homework 4 Solutions}
\author{Noah J. Cowan} 
\date{Fall 2014}
\begin{document}
\maketitle

\begin{enumerate}
\item The RR planar arm is described in Figure~\ref{fig:rrarm} with joint space variables $q_1$ and $q_2$.

\begin{figure}[h]
  \centering
  \includegraphics[width=2in]{./figs/rr-arm-2}
  \caption{A revoltue-revolute manipulator.}
  \label{fig:rrarm}
\end{figure}

\item See Figure~\ref{fig:rrarm} for the DH frames. The DH parameters are given by Table~\ref{tab:dh}.

\begin{table}[h]
  \centering
  \caption{DH parameters}
  \label{tab:dh}
  \begin{tabular}{ccccc}
    Link & $a_i$ & $\alpha_i$ & $d_i$ & $\theta_i$ \\
    \hline \hline
    1 & $\ell_1$ & 0 & 0 & $\theta_1^*$ \\
    2 & $\ell_2$ & 0 & 0 & $\theta_2^*$ \\
    \hline
  \end{tabular}
\end{table}

\item The full forward kinematic map is given by
\begin{align}
  T^0_2(\theta) &= \begin{bmatrix}
    \cos\theta_1 & -\sin\theta_1 & 0 & \ell_1\cos\theta_1 \\
    \sin\theta_1 & \cos\theta_1 & 0 & \ell_1\cos\theta_1 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
    \end{bmatrix}\begin{bmatrix}
    \cos\theta_2 & -\sin\theta_2 & 0 & \ell_2\cos\theta_2 \\
    \sin\theta_2 & \cos\theta_2 & 0 & \ell_2\cos\theta_2 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1    
    \end{bmatrix} \\
  &= \begin{bmatrix}
    \cos(\theta_1 + \theta_2) & -\sin(\theta_1 + \theta_2) & 0 & \ell_1\cos\theta_1 + \ell_2\cos(\theta_1 + \theta_2) \\
    \sin(\theta_1 + \theta_2) & \cos(\theta_1 + \theta_2) & 0 & \ell_1\cos\theta_1 + \ell_2\cos(\theta_1 + \theta_2) \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1    
    \end{bmatrix}.
\end{align}

\item Since $\dot R R^\intercal$ is a skew-symmetric matrix, it must be the case that $\dot R = \hat \omega R$ for some vector $\omega\in\mathbb{R}^3$. In this case, $R_2^0 = R^0_1R^1_2$, and, as such, $\dot R^0_2 = \dot R^0_1R^1_2 + R^0_1\dot R^1_2$ by the chain rule. Therefore, one can write
\begin{equation}
  \dot R^0_1 R^1_2 = \hat \omega^0_{0,1}R_2^0.
  \label{eq:1}
\end{equation}
Moreover, it is simple to see that
\begin{equation}
  \begin{split}
    R_1^0\dot R^1_2 &= R^0_1\hat\omega^1_2R^1_2 \\
     &= R^0_1\hat\omega^1_2 R^1_2 \\
     &= \hat{R^0_1\omega^1_{1,2}}R^0_1R^1_2 \\
     &= \hat{R^0_1\omega^1_{1,2}}R^0_2
  \end{split}
  \label{eq:2}
\end{equation}
Combining Eq.~\ref{eq:1} and~\ref{eq:2}, and using properties of the hat operator, we see that (4.27 in Spong)
\begin{equation}
  \omega^0_2 = \omega^0_{0,1} + R^0_1w^1_{1,2}.
\end{equation}

By inspection (or differentiation) we see that rotations of $\theta$ about the $z$-axis result in an angular velocity of $e_3\dot\theta$ for $e_3 = [0, 0, 1]^\intercal$. Thus, for the planar RR arm,
\begin{equation}
\boxed{
  \omega^0_2 = e_3\dot\theta_1 + R(\theta_1)^0_1e_3\dot\theta_2
}
\end{equation}

\item \begin{equation}
J_\omega(\theta) = \begin{bmatrix}
  0 & 0 \\
  0 & 0 \\
  1 & 1
\end{bmatrix}
\end{equation}
The dimensions of $J_\omega$ are $3 \times 2$.

\item For this problem, simply take the time derivative of the end-effector position:
\begin{align*}
\dot x &= -\ell_1\dot\theta_1\sin\theta_1 - \ell_2(\dot\theta_1+\dot\theta_2)\sin(\theta_1+\theta_2) \\
\dot y & = \ell_1\dot\theta_1\cos\theta_1 + \ell_2(\dot\theta_1+\dot\theta_2)\cos(\theta_1+\theta_2)
\end{align*}
The translation Jacobian, $J_v(\theta)\in\mathbb{R}^{3 \times 2}$ is then given by
\begin{equation}
\boxed{
  J_v(\theta) = \begin{bmatrix}
    -\ell_1\sin\theta_1 - \ell_2\sin(\theta_1+\theta_2) & -\ell_2\sin(\theta_1+\theta_2) \\
    \ell_1\cos\theta_1 + \ell_2\cos(\theta_1+\theta_2) & \ell_2\cos(\theta_1+\theta_2) \\
    0 & 0
  \end{bmatrix}
}
\end{equation}

\item Yes, $J_v(\theta)$ can be thought of as the matrix of partial derivatives of $o^0_2(\theta)$.

\item In general, this uses rotation matrices and does not have some physical meaning. However, in this problem, there is rotation only about the $z$-axis. As such, $J_\omega(\theta)$ can be though of as the matrix of partial derivatives of the function $g=[0,0,\theta_1 + \theta_2]^\intercal$, i.e., the rotation of the end-effector about the $z$-axis.

\item
\begin{enumerate}[(a)]
\item The typical rank of this matrix is 2. It is a $3 \times 2$ matrix, so the rank can be at most 2. In this case
\begin{equation*}
\begin{matrix}
\det(J_v(\theta)) = \ell_1\ell_2\sin(\theta_2),
\end{matrix}
\end{equation*}
which is zero only for $\theta_2= k\pi$ for an integer value of $k$.

\item Per the above, the set of all critical points is $\theta_2 = k\pi$ for an integer value of $k$. The set of critical points is drawn in Figure~\ref{fig:critPoints}.

\begin{figure}
  \centering
  \includegraphics[width=3in]{./figs/rr-arm-criticalPoints}
  \caption{The critical points for an RR arm.}
  \label{fig:critPoints}
\end{figure}

\item Pick the point $\theta = [\frac{\pi}{2}, \frac{\pi}{2}]^\intercal$. The Jacobian at this point is
\begin{equation}
J_v(\theta) = \begin{bmatrix}
  -\ell_1 & 0 \\
  -\ell_2 & -\ell_2 \\
  0 & 0
\end{bmatrix}.
\end{equation}
A basis for the kernel is the matrix $[0,0]^\intercal$. A basis for the image is the set $\lbrace [\ell_1,0,0]^\intercal, [\ell_2,\ell_2,0]^\intercal \rbrace$.

\item Pick one critical point $\theta = [0, 0]^\intercal$. The Jacobian at this point is
\begin{equation}
J_v(\theta) = \begin{bmatrix}
  0 & 0 \\
  \ell_1 + \ell_2 & \ell_2 \\
  0 & 0
\end{bmatrix}.
\end{equation}
A basis for the kernel is the matrix $\lbrace[\frac{-\ell_2}{\ell_1+\ell_2},1]^\intercal\rbrace$. A basis for the image is  $\lbrace [0,1,0]^\intercal\rbrace$. This point represents when the two links are co-linear of length $\ell_1 + \ell_2$.

\item Pick a second, distinct critical point $\theta = [\pi,\pi]^\intercal$.  The Jacobian at this point is
\begin{equation}
J_v(\theta) = \begin{bmatrix}
  0 & 0 \\
  -\ell_1 + \ell_2 & \ell_2 \\
  0 & 0
\end{bmatrix}.
\end{equation}
A basis for the kernel is the matrix $\lbrace[\frac{\ell_2}{\ell_1-\ell_2},1]^\intercal\rbrace$. A basis for the image is  $\lbrace [0,1,0]^\intercal\rbrace$. This point represents when the manipulator has turned back on itself.
\end{enumerate}

\item This can be derived in a number of ways. One can use the principle of virtual work, or simple physics. Recall that $\tau = r \times f$. Using $r$ as the end-effector position relative to each joint, we see that
\begin{equation} \boxed{
    \tau = J_v(\theta)^\intercal f.
}\end{equation}
\item As with before, either use virtual work or simple physics. From virtual work, $m_g\delta\theta = \tau\delta q$. Thus,
\begin{equation} \boxed{
    \tau = J_\omega(\theta)^\intercal m_g.
}\end{equation}

\item Combining the two previous equations, we see
\begin{equation} \boxed{
    \tau = J(\theta)^\intercal \begin{bmatrix}
      f_g \\
      m_g
    \end{bmatrix}
} \end{equation}
\end{enumerate}
\end{document}
