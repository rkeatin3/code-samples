\documentclass{article}
\usepackage{amsmath,amsfonts}
\input{../cowanhdr}
\usepackage[margin=1in]{geometry}
\usepackage[colorlinks=true]{hyperref}
\usepackage{enumerate}
\usepackage{graphicx}
\graphicspath{{figs/}}

\title{M.E. 530.646 Take-Home Midterm II (REV 1)}

\input{../author}

\date{Due: Friday 5 Dec 5PM} 
\begin{document}

\maketitle

Name: \underline{\hspace{5in}}

\vspace{0.25in}
Instructions:
\begin{itemize}
\item Open book.
\item Open note. 
\item Open Internet. 
\item Open library.
\item Open brain.
\item Closed any other human being. Period. 
\item Unlike the midterm, no free points for leaving stuff blank.
\item Before handing this in, please read and sign the honor code,
  below.
\end{itemize}


Honor code: I hereby certify that I did not give nor receive aid on
this exam, nor did I witness any aid being given or received.

\vspace{0.3in}
Sign:\footnote{If you gave, received, or saw aid being given or
  received, please do not sign.}  \underline{\hspace{5in}}

\clearpage

\section*{Introduction}
The B\"ugler arm is an awesome machine that was built in the late
1980's to juggle:
\begin{quote}
  \href{http://www.youtube.com/watch?v=u8I7EXXgTvk}{http://www.youtube.com/watch?v=u8I7EXXgTvk}
\end{quote}
You can read a bit about the robot and see a picture of it in these
papers if you wish
\cite{rizziactive1996,whitcombcomparative1993,rizzidistributed1992}
(not required for the problems!). The robot was built by Al Rizzi
and Louis Whitcomb and was named after Martin B\"uhler, who built
the first truly dynamic juggling robot. Martin is one of the great
pioneers in robotics:
\begin{quote}
  \href{http://scholar.google.com/citations?user=V4i_fxkAAAAJ&hl=en}{Martin
    B\"uhler's Google Scholar Page}
\end{quote}
He left academics and has headed up R\&D at places like iRobot. He's
now with Vecna I believe. Anyway, this is the machine that inspired
me to get into robotics so I thought we could have some fun and
analyze it.  Several of the questions in this exam focus on this
unusual robot. But note, the problems do not relate to juggling.   The robot is pictured here:

\begin{figure}[h!]
  \centering
  \includegraphics{buehgler}
  \caption{The Buehgler arm, with frames (roughly) as depicted in the
    original papers.}
\end{figure}

Notes:
\begin{itemize}
\item The key thing to note about this figure is not the details of
  the frame assignment, but how the axes intersect. Lastly note that
  there are only 3 actuated degrees of freedom; that is, there are
  three motors.
\item Since the goal was to simply juggle the ball, one gets a fourth
  ``passive'' degree of freedom for free, namely the ball can land
  anywhere along the length of the paddle, depicted as $q_4$,
  which measures the distance along the paddle (that is, measured
  along the 3rd motor axis), measured from intersection of the final
  two motor axes.
\item Note that when the virtual joint
  $q_4=0$, the origins of frames $\{2\}$ and $\{3\}$ are
  coincident.
\item I've labeled these frames as 0 through 3, but you may need to
  reassign frames for the Spong DH convention; see next problem. I'm
  not saying they are right or wrong, but I simply copied these frames
  right out of the literature.  
\item In some problems I will ``Freeze'' certain joint variables,
  reducing the number of degrees of freedom, and allow others,
  including sometimes pretending that the passive 4th of freedom is a
  real DOF. Please read each problem carefully.

\item 
  \begin{tabular}[h]{cc}
    Parameter & Default value\\
    \hline 
    \hline 
    $\ell_1$ & 1m\\
    \hline
    $\ell_2$ & 0.5m\\
    \hline 
  \end{tabular}
\end{itemize}

\clearpage

\begin{enumerate}
\item (20 pts; 5 pts per part) Kinematics.
  \begin{enumerate}
  \item Using the DH convention in Spong et al.~\cite{spongrobot2006},
    compute the forward kinematics, taking into account the
    ``passive'' fourth degree of freedom. (Essentially, pretend there
    is a prismatic joint aligned along the axis of the third motor,
    and assume the ``gripper'' is at the end of that joint.) The final
    answer here should be two things: a DH parameter table and a
    homogenous transform matrix, $T^0_4(q)$ (\textbf{corrected}) that
    is a function of all 4 joint variables (including the passive
    one), namely $q=(q_1,q_2,q_3,q_4)^T\in T^3\times \Real$. (Note
    that $T^3 = S^1 \times S^1 \times S^1$ simply means the first
    three joints are angles, and $\Real$ indicates that the last joint
    variable is prismatic.)

  \item Compute the Jacobian matrix. Break it up into the translation
    part, $J_v$ and the revolute part, $J_\omega$, so that
    \begin{equation*}
      \begin{bmatrix}
        v \\
        \omega
      \end{bmatrix}
      =
      \begin{bmatrix}
        J_v\\
        J_\omega
      \end{bmatrix}
      \dot q
    \end{equation*}
    Follow the Spong et al.~\cite{spongrobot2006} conventions for what
    $v$ and $\omega$ refer to.


  \item Assume that the robot is massless. Also, assume the robot is
    in static equilibrium and holding a $m=1kg$ mass sitting out on
    the paddle a distance $q_4$ along the paddle, perfectly
    aligned along the third motor axis. (We assume the plane of the
    paddle contains the third motor axis.)  Assume that gravity is
    aligned downward in the obvious way, and assume that the mass only
    applies a pure force (no torque) due to gravity. Compute a general
    expression for the joint torques required to maintain static
    equilibrium, without plugging in any numbers for the $q$'s or
    $d$'s. Simplify your expression as much as possible.

  \item Now, using the result from the previous part, assume
    $q=(0,\frac{\pi}{4},q_3,1\,\mathrm{m})$ and, assuming
    $\ell_1=1m$, $\ell_2=0.5m$, compute specific numbers for the forces and
    torques as a function of $q_3$ which is left as a free
    variable. EXPLAIN. (A simple sketch may help.)
  \end{enumerate}
  

  \clearpage
\item (10 pts; 5pts per part) Kinematic analysis and singularities. For this problem,
  assume that $q_4=1m=\mathrm{const.}$, and treat the vector
  $q=(q_1,q_2,q_3)\in T^3$. In other words, in
  this problem, we have a 3DOF robot. To summarize:

  \begin{quote}
    \begin{tabular}[h]{cc}
      Parameter & value for this problem\\
      \hline 
      \hline 
      $\ell_1$ & 1m\\
      \hline
      $\ell_2$ & 0.5m\\
      \hline 
      $q_1$ & free\\
      \hline 
      $q_2$ & free\\
      \hline 
      $q_3$ & free \\
      \hline 
      $q_4$ & 1 m\\
      \hline 
    \end{tabular}
  \end{quote}


  \begin{enumerate}
  \item For this part, consider only the translational part,
    $J_v$. What is the ``typical'' rank of $J_v$? That is, for
    ``most'' configurations (as if you were to select one randomly)
    what is the rank? Characterize any singularities in terms of
    $J_v$. \emph{Explain your results in clear and precise terms.}

  \item For this part, consider only the \emph{rotational} part,
    $J_\omega$. What is the ``typical'' rank of $J_\omega$? That is,
    for ``most'' configurations (as if you were to select one
    randomly) what is the rank? Characterize any singularities in
    terms of $J_\omega$. \emph{Explain your results in clear and precise terms.}
  \end{enumerate}

  \vspace{2em}
\item (10 pts; 5pts per part) Kinematic analysis and singularities,
  revisited. For this problem, assume that $q_3=0=\mathrm{const.}$,
  and treat the vector $q=(q_1,q_2,q_4)\in T^2\times \Real$. In other
  words, in this problem, we have a 3DOF robot. To summarize:

  \begin{quote}
    \begin{tabular}[h]{cc}
      Parameter & value for this problem\\
      \hline 
      \hline 
      $\ell_1$ & 1m\\
      \hline
      $\ell_2$ & 0.5m\\
      \hline 
      $q_1$ & free\\
      \hline 
      $q_2$ & free\\
      \hline 
      $q_3$ &0 \\
      \hline 
      $q_4$ & free\\
      \hline 
    \end{tabular}
  \end{quote}



  \begin{enumerate}
  \item For this part, consider only the translational part,
    $J_v$. What is the ``typical'' rank of $J_v$? That is, for
    ``most'' configurations (as if you were to select one randomly)
    what is the rank? Characterize any singularities in terms of
    $J_v$. \emph{Explain your results in clear and precise terms.}

  \item For this part, consider only the \emph{rotational} part,
    $J_\omega$. What is the ``typical'' rank of $J_\omega$? That is,
    for ``most'' configurations (as if you were to select one
    randomly) what is the rank? Characterize any singularities in
    terms of $J_\omega$. \emph{Explain your results in clear and precise terms.}
  \end{enumerate}


  \clearpage
\item (20 pts) For this problem, assume that $q_3=0=\mathrm{const}$ and
  $q_4=1m=\mathrm{const.}$, and treat the vector
  $q=(q_1,q_2)\in T^2$. In other words, in this
  problem, we have a 2DOF robot.  Furthermore, set $\ell_2=0$. To summarize:
  \begin{quote}
    \begin{tabular}[h]{cc}
      Parameter & value for this problem\\
      \hline 
      \hline 
      $\ell_1$ & 1m\\
      \hline
      $\ell_2$ & 0m\\
      \hline 
      $q_1$ & free\\
      \hline 
      $q_2$ & free\\
      \hline 
      $q_3$ & 0 rad\\
      \hline 
      $q_4$ & 1 m\\
      \hline 
    \end{tabular}
  \end{quote}

  Now attach a mass, $M = 1Kg$ to the robot at $q_4=1m$. Assume
  the rest of the robot is COMPLETELY massless. This is a bit of a
  silly assumption, because the motors on the B\"uhgler are actually
  huge, heavy monsters.  But the dynamics become nasty to do by hand
  otherwise! Do all calculations with symbols and do not plug in
  numbers until the very end. 

  \begin{enumerate}
  \item (5 pts) Compute the potential energy $V$.
  \item (5 pts) Compute the kinetic energy $T$.
    
  \item (10 pts) Write down the Lagrangian and compute the Lagrange--Euler equations of motion. Give a final
    expression. 
  \end{enumerate}


\end{enumerate}

\vspace{2in}

\bibliographystyle{abbrv}
\bibliography{../me530646}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
