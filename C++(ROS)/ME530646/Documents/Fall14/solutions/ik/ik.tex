\documentclass[12pt, letterpaper]{article}

\input{../../hdr530646}
\usepackage[parfill]{parskip}
\usepackage{subfigure}
\usepackage{amsmath}


\DeclareMathOperator*{\atan2}{atan2}

\graphicspath{{images/}}


\title{ M.E. 530.646\\
UR5 Inverse Kinematics}
\date{}

\author{Ryan Keating\\Johns Hopkins University}

\begin{document}

\maketitle
\setlength\parindent{0pt}

\section*{Introduction}
Figure \ref{Frames} below illustrates a common assignment of Denavit-Hartenberg convention to the UR5 robot (shown with all joint angles at 0).

\begin{figure}[h]
\centering
\includegraphics[width=5in]{FrameAssignment}
\caption{D-H Convention Frame Assignment}
\label{Frames}
\end{figure}

\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|c|}
  \hline                       
  Joint &   $a$    & $\alpha$ &   $d$   &   $\theta$   \\
  \hline
    1   &     0    &  $\pi/2$  & $0.089159$ & $\theta_1$ \\
    2	& $-0.425$ &     0     &    0    & $\theta_2$ \\
  	3	& $-0.39225$ &     0     &    0    & $\theta_3$ \\
  	4   &     0    &  $\pi/2$  & $0.10915$ & $\theta_4$ \\   
  	5   &     0    &  $-\pi/2$ & $0.09465$ & $\theta_5$ \\  
  	6   &     0    &     0     & $0.0823$ & $\theta_6$ \\ 
  \hline  
\end{tabular}
\caption{D-H Parameters}
\label{DH Params}
\end{table}

As with any 6-DOF robot, the homogeneous transformation from the base frame to the gripper can be defined as follows:

\begin{equation}
T^0_6(\theta_1,\theta_2,\theta_3,\theta_4,\theta_5,\theta_6)
= 
T^0_1(\theta_1)\:T^1_2(\theta_2)\:T^2_3(\theta_3)\:T^3_4(\theta_4)\:T^4_5(\theta_5)\:T^5_6(\theta_6) 
\end{equation}

Also remember that a homogenous transformation $T^i_j$ has the following form:

\begin{equation}
T^i_j = 
\left[\begin{array}{cc}
R^i_j & \vec{P^i_j}\\
0 & 1\\
\end{array}\right]
=
\left[\begin{array}{cccc} 
x_x & y_x & z_x & (P^i_j)_x \\  
x_y & y_y & z_y & (P^i_j)_y  \\ 
x_z & y_z & z_z & (P^i_j)_z  \\
  0   &   0   &   0   &        1    
\end{array}\right]
\label{Tij}
\end{equation}

where $\vec{P^i_j}$ is the translation from frame $i$ to frame $j$ and each column of $R^i_j$ is the projection of one of the axes of frame $j$ onto the axes of frame $i$ (i.e. $[x_x,x_y,x_z]^T\equiv x^i_j$).

.\cite{spongrobot2006}

\section*{Inverse Kinematics}
The first step to solving the inverse kinematics is to find $\theta_1$. To do so, we must first find the location of the $5^{th}$ coordinate frame with respect to the base frame, $P^0_5$. As illustrated in Figure \ref{P5} below, we can do so by translating by $d_6$ in the negative $z$ direction from the $6^{th}$ frame.

\begin{figure}[h]
\centering
\includegraphics[width=4in]{FindingP5}
\caption{Finding the Origin of the $5^{th}$ Frame}
\label{P5}
\end{figure}

This is equivalent to the following operation:
\begin{equation}
\vec{P^0_5} =T^0_6\left[\begin{array}{c} 0 \\ 0 \\ -d_6 \\ 1\end{array}\right]-\left[\begin{array}{c} 0 \\ 0 \\ 0 \\ 1\end{array}\right]
\end{equation}

With the location of the $5^{th}$ frame, we can draw an overhead view of the robot:

\begin{figure}[!h]
\centering
\includegraphics[width=4in]{Theta1}
\caption{Finding $\theta_1$}
\label{Theta1}
\end{figure}

From Figure \ref{Theta1}, we can see that $\theta_1 = \psi + \phi + \frac{\pi}{2}$ where

\begin{equation}
\psi = \atan2\left((P^0_5)_y,(P^0_5)_x\right)
\end{equation}

\begin{equation}
\phi = \pm\,\arccos\left(\frac{d_4}{(P^0_5)_{xy}}\right) = 
\pm\,\arccos\left(\frac{d_4}{\sqrt{(P^0_5)_x\,^2 + (P^0_5)_y\,^2 }}\right)
\label{phi}
\end{equation}

The two solutions for $\theta_1$ correspond to the shoulder being either ``left'' or ``right,''. Note that Equation \ref{phi} has a solution in all cases except that $d_4 > (P^0_5)_{xy}$. You can see from Figure \ref{Theta1} this happens when the origin of the $3^{rd}$ frame is close to the $z$ axis of frame $0$. This forms an unreachable cylinder in the otherwise spherical workspace of the UR5 (as shown in Figure \ref{Workspace} taken from the UR5 manual).

\begin{figure}[!h]
\centering
\includegraphics[width=5in]{Workspace}
\caption{The Workspace of the UR5}
\label{Workspace}
\end{figure}

Knowing $\theta_1$, we can now solve for $\theta_5$. Once again we draw an overhead view of the robot, but this time we consider location of the $6^{th}$ frame with respect to the $1^{st}$ (Figure \ref{Theta5}.

\begin{figure}[h]
\centering
\includegraphics[width=4in]{Theta5}
\caption{Finding $\theta_5$}
\label{Theta5}
\end{figure}

We can see that $(P^1_6)_z = d_6\cos(\theta_5) + d_4$, where $(P^1_6)_z = (P^0_6)_x\sin(\theta_1)-(P^0_6)_y\cos(\theta_1)$. Solving for $\theta_5$,

\begin{equation}
\theta_5 = \pm\;\arccos\left(\frac{(P^1_6)_z - d_4}{d_6}\right)
\label{q5}
\end{equation}

Once again, there are two solutions. These solutions correspond to the wrist being ``down'' and ``up.''


Figure \ref{Theta6} illustrates that, ignoring translations between frames, $z_1$ can be represented with respect to frame 6 as a unit vector defined with spherical coordinates. We can find the x and y components of of this vector by projecting it onto the x-y plane and then onto the x or y axes. 

Next we find transformation from frame 6 to frame 1,

\begin{equation}
T^6_1 = ((T^0_1)^{-1}\;T^0_6)^{-1}
\end{equation}

Remembering the structure the of the first three columns of the homogenous transformation $T^6_1$(see Equation \ref{Tij}), we can form the following equalities:

\begin{equation}
-\sin(\theta_6)\sin(\theta_5) = z_y
\end{equation}

\begin{equation}
\cos(\theta_6)\sin(\theta_5) = z_x
\end{equation}

Solving for $\theta_6$,

\begin{equation}
\theta_6 = \atan2\left(\frac{-z_y}{\sin(\theta_5)},\frac{z_x}{\sin(\theta_5)}\right)
\label{q6}
\end{equation}

Equation \ref{q6} shows that $\theta_6$ is not well-defined when $\sin(\theta_5) = 0$ or when $z_x,z_y = 0$. We can see from Figure \ref{Theta6} that these conditions are actually the same. In this configuration joints 2, 3, 4, and 6 are parallel. As a result, there four degrees of freedom to determine the position and rotation of the end-effector in the plane, resulting in an infinite number of solutions. In this case, a desired value for $q_6$ can be chosen to reduce the number of degrees of freedom to three.


\begin{figure}[h]
\centering
\includegraphics[width=4in]{Theta6}
\caption{Finding $\theta_6$}
\label{Theta6}
\end{figure}

We can now consider the three remaining joints as forming a planar 3R manipulator. First we will find the location of frame 3 with respect to frame 1. This is done as follows:

\begin{equation}
T^1_4 = T^1_6\;T^6_4 = 
T^1_6\;(T^4_5\;T^5_6)^{-1}
\end{equation}

\begin{equation}
\vec{P^1_3} = T^1_4\;\left[\begin{array}{c} 0 \\ -d_4 \\ 0 \\ 1\end{array}\right] - \left[\begin{array}{c} 0 \\ 0 \\ 0 \\ 1\end{array}\right]
\end{equation}

Now we can draw the plane containing frames 1-3, as shown in Figure \ref{Theta23}. We can see that 

\begin{equation}
\cos(\xi) = \frac{||\vec{P^1_3}||^2-a_2^2-a_3^2}{2a_2a_3}
\label{Theta31}
\end{equation}
with use of the law of cosines.

\begin{equation}
\begin{array}{r@{}l}
  \cos(\xi) {}=  -\cos(\pi-\xi)\\
  {}=   -\cos(-\theta_3)\\
  {}=   \cos(\theta_3)\\
\end{array}
\label{Theta32}
\end{equation}

Combining \ref{Theta31} and \ref{Theta32}
\begin{equation}
\theta_3 = \pm \arccos\left(\frac{||\vec{P^1_3}||^2-a_2^2-a_3^2}{2a_2a_3}\right)
\label{q3}
\end{equation}

Equation \ref{q3} has a solution as long as the argument to $\arccos$ is $\in [-1,1]$. We can see that large values of $||\vec{P^1_3}||$ will cause this the argument to exceed 1. The physical interpretation here is that the robot can only reach so far out in any direction (creating the spherical bound to the workspace as seen in \ref{Workspace}).

Figure \ref{Theta23} also shows that 
\begin{equation}
  \theta_2 = -(\delta-\epsilon)
  \label{Theta21}
\end{equation}

where $\delta = \atan2((P^1_3)_y,-(P^1_3)_x)$ and $\epsilon$ can be found via law of sines:
\begin{equation}
  \frac{\sin(\xi)}{||\vec{P^1_3}||} = \frac{\sin(\epsilon)}{a_3}
  \label{Theta22}
\end{equation}

Combining \ref{Theta21} and \ref{Theta22}
\begin{equation}
  \theta_2 = -\atan2((P^1_3)_y,-(P^1_3)_x)+\arcsin\left(\frac{a_3\sin(\theta_3)}{||\vec{P^1_3}||}\right)
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[width=4in]{Theta23}
\caption{Finding $\theta_2$ and $\theta_3$}
\label{Theta23}
\end{figure}

Notice that there are two solutions for $\theta_2$ and $\theta_3$. These solutions are known as ``elbow up'' and ``elbow down.''

The final step to solving the inverse kinematics is to solve for $\theta_4$. First we want to find $T^3_4$:

\begin{equation}
T^3_4 = T^3_1\;T^1_4=(T^1_2\;T^2_3)^{-1}T^1_4
\end{equation}

Using the first column of $T^3_4$,
\begin{equation}
\theta_4 = \atan2(x_y,x_x)
\end{equation}

Below are figures which show the eight solutions for one end-effector position/orientation.

\begin{figure}[h]
\centering
\subfigure[Shoulder Left, Elbow Up, Wrist Down]{\includegraphics[width=3in]{Sol1}}
\subfigure[Shoulder Left, Elbow Up, Wrist Up]{\includegraphics[width=3in]{Sol3}}
\subfigure[Shoulder Left, Elbow Down, Wrist Down]{\includegraphics[width=3in]{Sol2}}
\subfigure[Shoulder Left, Elbow Down, Wrist Up]{\includegraphics[width=3in]{Sol4}}
\subfigure[Shoulder Right, Elbow Down, Wrist Up]{\includegraphics[width=3in]{Sol5}}
\subfigure[Shoulder Right, Elbow Down, Wrist Down]{\includegraphics[width=3in]{Sol7}}
\subfigure[Shoulder Right, Elbow Up, Wrist Up]{\includegraphics[width=3in]{Sol6}}
\subfigure[Shoulder Right, Elbow Up, Wrist Down]{\includegraphics[width=3in]{Sol8}}
\end{figure}

\let\thefootnote\relax\footnotetext{This solution was adapted from an existing solution written by Kelsey P. Hawkins
  } 

\end{document}
