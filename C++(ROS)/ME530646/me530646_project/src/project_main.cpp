#include <Eigen/Core>
#include <unsupported/Eigen/MatrixFunctions>
#include <ur5/utilities.h>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <me530646_lab3/lab3.h>
#include <me530646_lab4/lab4.h>

#define PI M_PI


Eigen::VectorXd LinearInterpolation(const Eigen::VectorXd &qC, const Eigen::Matrix4d &Tf)
{
	//Get matrix of solutions to inverse for the final transformation
	Eigen::MatrixXd q_sol;
	int num_sol = inverse(Tf,q_sol);

	//Find the closest solution to the current joint configuration
	double bestDist = 100000;
	Eigen::VectorXd closestSol(6);
	Eigen::VectorXd sol(6);
	for(int i = 0; i < num_sol; i++)
	{
		sol = q_sol.block<6,1>(0,i);
		double dist = (qC - sol).norm();
		if(dist < bestDist)
		{
			bestDist = dist;
			closestSol = sol;
		}
	}
	//Calculate and normalize error (to ensure constant step size)
	Eigen::VectorXd err = (qC - closestSol);
	err *= (1/err.norm());
	//Take step towards closest solution
	double epsilon = .02;
	Eigen::VectorXd qNew = qC - epsilon*err;
	return qNew;
}

Eigen::VectorXd cartVel(const Eigen::Matrix4d &Tc, const Eigen::Matrix4d &Tf)
{
	//Rotation and translation of current transformation from base
	Eigen::Matrix3d Rc = Tc.block<3,3>(0,0);
	Eigen::Vector3d pc = Tc.block<3,1>(0,3);
	//Rotation and translation of final transformation from base
	Eigen::Matrix3d Rf = Tf.block<3,3>(0,0);
	Eigen::Vector3d pf = Tf.block<3,1>(0,3);

	//Gains
	double kv = .9;
	double kw = .6;

	//Error
	Eigen::Matrix3d w_hat = (Rc*(Rf.transpose())).log();
	Eigen::Vector3d w(w_hat(2,1),w_hat(0,2),w_hat(1,0));
	Eigen::Vector3d v = pc-pf;
	printEigen(w);
	printEigen(v);
	//Feedback
	v*= -kv;
	Eigen::Vector3d omega = -kw*w;		
	Eigen::VectorXd vel(6);
	//Create vector of velocity in generalized Cartesian coordinates
	vel.block<3,1>(0,0) = v;
	vel.block<3,1>(3,0) = omega;
	//Normalize to ensure constant step size
	vel *= (1/vel.norm());
	return vel;
}

Eigen::VectorXd JacobianInverse(const Eigen::VectorXd &qC, const Eigen::Matrix4d &Tf)
{
	//Get current transformation
	Eigen::Matrix4d Tc = UR5::fwd(qC);	

	//Get velocities in Cartesian space
	Eigen::VectorXd v = cartVel(Tc,Tf);

	//Map into joint space
	//q_k+1 = q_k+eps*q' where q' = J^-1*v
	double epsilon = .01;
	Eigen::VectorXd qNew;
	if(fabs(J(qC).determinant()) > .005) 
		qNew = qC + epsilon*J(qC).inverse()*v;
	//Use transpose when close to a singularity
	else
		qNew = qC + 2.5*epsilon*J(qC).transpose()*v;
	return qNew;
}	

Eigen::VectorXd JacobianTranspose(const Eigen::VectorXd &qC, const Eigen::Matrix4d &Tf)
{
	//Get current transformation
	Eigen::Matrix4d Tc = UR5::fwd(qC);	

	//Get velocities in Cartesian space
	Eigen::VectorXd v = cartVel(Tc,Tf);

	//Map into joint space
	//q_k+1 = q_k+eps*q' where q' = J^T*v
	double epsilon = .01;
	Eigen::VectorXd qNew = qC + epsilon*J(qC).transpose()*v;
	return qNew;
}

int main(int argc, char **argv){

	//Checking command line arguments
	if(argc != 3){
		ROS_ERROR("Please enter \"Linear Interpolation\" \"Jacobian Inverse\" or \"Jacobian Transpose\" for the first argument and \"Rviz\" or \"Real\" for the second.\n");
		exit(EXIT_FAILURE);
	}
	//Creating function handle for correct control method
	Eigen::VectorXd (*method)(const Eigen::VectorXd &qC, const Eigen::Matrix4d &Tf);
	
	//Assigning correct function based on input
	std::string control = std::string(argv[1]);
	if(control.compare("LinearInterpolation") == 0)
		method = &LinearInterpolation;
	else if(control.compare("JacobianInverse") == 0)
		method = &JacobianInverse;
	else if(control.compare("JacobianTranspose") == 0)
		method = &JacobianTranspose;
	else
	{
		ROS_ERROR("Please enter \"LinearInterpolation\" \"JacobianInverse\" or \"JacobianTranspose\" for the first argument.\n");
		exit(EXIT_FAILURE);
	}

	//Assigning value to sim based on input 
	std::string rob = std::string(argv[2]);
	double sim;
	if (rob.compare("Rviz") == 0)
		sim = 1;
	else if(rob.compare("Real") == 0)
		sim = 0;
	else
	{
		ROS_ERROR("Please enter \"Rviz\" or \"Real\" for the second argument.\n");
		exit(EXIT_FAILURE);
	}

	//Initialize ros, create UR5 instance
	ros::init(argc,argv,"lab3");
	ros::NodeHandle n;
	UR5 robot = UR5(n);

	//Create initial and final joint vectors 
	Eigen::VectorXd q0(6);
	q0 << 1,6,-4,2,5,2;
	Eigen::Matrix4d T0 = robot.fwd(q0);
	Eigen::VectorXd qf(6);
	qf << 3,4,2,-1,0,2;

	//Find final transformation
	Eigen::Matrix4d Tf = robot.fwd(qf);
	Eigen::Matrix3d Rf = Tf.block<3,3>(0,0);
	Eigen::Vector3d pf = Tf.block<3,1>(0,3);

	//Put the robot in its initial position
	if(sim)
		robot.plotfwd(q0);
	else{
		robot.movefwd(q0);
		//Leave extra time for real robot to move 
		sleep(3);
	}
	sleep(3);

	//Get current position
	Eigen::VectorXd qC(6);
	robot.getPos(qC);
	Eigen::Matrix4d Tc = robot.fwd(qC);

	//While not at the final transformation
	while((Tc-Tf).norm() > .01)
	{
		//Get current position
		robot.getPos(qC);
		Tc = robot.fwd(qC);	

		//Get new joint command
		Eigen::VectorXd qNew = method(qC,Tf);

		//Send robot to new position
		if(sim)
		{
			robot.plotfwd(qNew);
		}
		else
			robot.movefwd(qNew);
	}


}