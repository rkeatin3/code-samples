#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <iostream>
#include <sstream>

// socket includes [probably more than necessary]
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h> // for close


// TCP/IP code from 
// TCP/IP Sockets in C
// http://cs.ecs.baylor.edu/~donahoo/practical/CSockets/textcode.html

int clntSocket = -1;                     /* Socket descriptor for client */

void ur5Callback(const sensor_msgs::JointState::ConstPtr & msg)
{
  if(clntSocket < 0)
    ROS_ERROR("bad client");

  if((clntSocket < 0) || (msg->position.size() < 6)) {
    ROS_ERROR("bad message");
    return;
  }

  std::stringstream cmd;
  cmd << "( " << msg->position[0];
  for(int i=1; i<6; i++) {
    cmd << ", " << msg->position[i];
  }
  cmd << ")";

  std::string str(cmd.str());
  ROS_INFO("%s",str.c_str());

  if (send(clntSocket, str.c_str(), str.length(), 0) == -1)
    clntSocket = -1;

}

int createSocket(unsigned short port)
{
  int sock;                        /* socket to create */
  struct sockaddr_in echoServAddr; /* Local address */

  /* Create socket for incoming connections */
  if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    ROS_ERROR("Could not create socket");
      
  /* Construct local address structure */
  memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
  echoServAddr.sin_family = AF_INET;                /* Internet address family */
  echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
  echoServAddr.sin_port = htons(port);              /* Local port */

  /* Bind to the local address */
  if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
    ROS_ERROR("Could not bind socket");
  
  /* Mark the socket so it will listen for incoming connections */
  if (listen(sock, 5) < 0)
    ROS_ERROR("Could not listen on socket");

  return sock;
}

int main(int argc, char **argv) 
{
  ROS_WARN("ur5_control main.\n"); 
  ros::init(argc, argv, "ur5_control");
  ros::NodeHandle n;
  unsigned short port = 5555;

  ros::Subscriber sub = n.subscribe("ur5_joint_state", 10, &ur5Callback);

  int sock = createSocket(port);
  struct sockaddr_in echoClntAddr; /* Client address */
  unsigned int clntLen;            /* Length of client address data structure */

  /* Set the size of the in-out parameter */
  clntLen = sizeof(echoClntAddr);
    
  ROS_WARN("Waiting for client.\n");  
  /* Wait for a client to connect */
  if ((clntSocket = accept(sock, (struct sockaddr *) &echoClntAddr, 
    		 &clntLen)) < 0)
    ROS_ERROR("Could not accept client connection");
    
  ros::Rate r(50);
  while(ros::ok())
  {
    ros::spin();
    r.sleep();
  }

  // close the sockets
  if(clntSocket >= 0)
    close(clntSocket);
  close(sock);
}
