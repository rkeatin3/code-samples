#include <ros/ros.h>

#include <ur5/robot_interface.h>
#include <sensor_msgs/JointState.h>
#include <iostream>
#include <fstream>

#include <cstring>

// Sockets
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <unistd.h>     /* for close() */

/***************
 * Robot State *
 ***************/
  union rm {
    char raw[sizeof(RobotModeData)];
    RobotModeData rmd;
  } robotMode;

  union jd {
    char raw[sizeof(JointData)];
    JointData jd;
  } jointData;

  union td{
    char raw[sizeof(ToolData)];
    ToolData tool;
  } toolData;

  union mbd{
    char raw[sizeof(MasterboardData)];
    MasterboardData data;
  } masterboardData;

  union mbde{
    char raw[sizeof(MasterboardDataWithEuromap)];
    MasterboardDataWithEuromap data;
  } masterboardDataWithEuromap;

  union ci{
    char raw[sizeof(CartesianInfo)];
    CartesianInfo info;
  } cartesianInfo;

  union cd{
    char raw[sizeof(ConfigurationData)];
    ConfigurationData config;
  } configurationData;

  union fmd{
    char raw[sizeof(ForceModeData)];
    ForceModeData fd;
  } forceModeData;

  union ai{
    char raw[sizeof(AdditionalInfo)];
    AdditionalInfo info;
  } additionalInfo;

/***************
 * Conversions *
 **************/
int unionValue(intUnion u)
{
  intUnion tmp;
  for(int i=0; i<4; i++) 
    tmp.raw[3-i] = u.raw[i];

  return tmp.data;
}

float unionValue(floatUnion u)
{
  floatUnion tmp;
  for(int i=0; i<4; i++)
    tmp.raw[3-i] = u.raw[i];

  return tmp.data;
}

double unionValue(doubleUnion u)
{
  doubleUnion tmp;
  for(int i=0; i<8; i++)
    tmp.raw[7-i] = u.raw[i];

  return tmp.data;
}

/****************
 * Socket calls *
 ***************/
void readPacket(const char *buf, const int len)
{

  if(buf[0] != char(16)) {
    std::cerr << "Unexpected packet type" << std::endl;
    return;
  }
  buf++;
  int readBytes = 1;
  int bytesToRead = 0;
  intUnion packLen;
  int packType;

  // read in each individual packet
  while(readBytes < len) {
    memcpy(&packLen.raw[0], &buf[0], 4);
    packType = buf[4];
    bytesToRead = unionValue(packLen);

    switch(packType) {
    case ROBOT_MODE_DATA:
      memcpy(&robotMode.raw[0], &buf[0], bytesToRead);
      break;
    case JOINT_DATA:
      memcpy(&jointData.raw[0], &buf[0], bytesToRead);
      break;
    case TOOL_DATA:
      memcpy(&toolData.raw[0], &buf[0], bytesToRead);
      break;
    case MASTERBOARD_DATA:
      if(bytesToRead == 72)
        memcpy(&masterboardData.raw[0], &buf[0], bytesToRead);
      else
        memcpy(&masterboardDataWithEuromap.raw[0], &buf[0], bytesToRead);
      break;
    case CARTESIAN_INFO:
      memcpy(&cartesianInfo.raw[0], &buf[0], bytesToRead);
      break;
    case KINEMATICS_INFO:
      // nothing to do here
      break;
    case CONFIGURATION_DATA:
      memcpy(&configurationData.raw[0], &buf[0], bytesToRead);
      break;
    case FORCE_MODE_DATA:
      memcpy(&forceModeData.raw[0], &buf[0], bytesToRead);
      break;
    case ADDITIONAL_INFO:
      memcpy(&additionalInfo.raw[0], &buf[0], bytesToRead);
      break;
    case CALIBRATION_DATA:
      // nothing to do here
      break;
    default:
      std::cerr << "Unrecognized packet " << packType << std::endl;
      return;
    }

    readBytes += bytesToRead;
    buf += bytesToRead;
  }
}

void receive(int sock)
{

  intUnion len;
  len.data = 0;
  int nBytes = 0;
  if((nBytes = recv(sock, &len.raw[0], 4, 0)) <= 0){
    // bad packet
    ROS_ERROR("Bad packet length\n");
    return;
  }

  int packetLength = unionValue(len);
  if((packetLength <= 4) || (packetLength > 4096))
    return;

  // read the packet, less the 4 bytes for length
  char buf[4096];
  int packetBytes = packetLength - 4;
  nBytes = recv(sock, &buf[0], packetBytes, 0);
  if(nBytes != packetBytes) {
    // bad packet
    ROS_ERROR("Bad packet\n");
    return;
  }

  readPacket(buf, packetBytes);
}

/************
 * Helpers *
 ***********/

std::vector<double> getPositions()
{
  std::vector<double> pos;
  pos.resize(6);
  for(int i=0; i<6; i++){
    pos[i] = unionValue(jointData.jd.joint[i].q_actual);
  }
  return pos;
}

void getToolPose(double pose[6])
{
  pose[0] = unionValue(cartesianInfo.info.x);
  pose[1] = unionValue(cartesianInfo.info.y);
  pose[2] = unionValue(cartesianInfo.info.z);
  pose[3] = unionValue(cartesianInfo.info.Rx);
  pose[4] = unionValue(cartesianInfo.info.Ry);
  pose[5] = unionValue(cartesianInfo.info.Rz);
}

int main(int argc, char **argv) {

  // define the socket variables
  int sock;
  struct sockaddr_in server;
  unsigned short port = 30002;
  std::string ur5IP = "127.0.0.1";//"172.22.22.2";//Robot IP
  std::string program;

  // initialize some variables
  memset(&jointData.raw[0], 0, sizeof(jointData));
  memset(&robotMode.raw[0], 0, sizeof(robotMode));
  memset(&toolData.raw[0], 0, sizeof(toolData));
  memset(&masterboardData.raw[0], 0, sizeof(masterboardData));
  memset(&masterboardDataWithEuromap.raw[0], 0, sizeof(masterboardDataWithEuromap));
  memset(&cartesianInfo.raw[0], 0, sizeof(cartesianInfo));
  memset(&configurationData.raw[0], 0, sizeof(configurationData));
  memset(&forceModeData.raw[0], 0, sizeof(forceModeData));
  memset(&additionalInfo.raw[0], 0, sizeof(additionalInfo));

  ros::init(argc, argv, "robot_interface");
  ros::NodeHandle n;

  // publisher to publish the current robot configuration
  ros::Publisher pub = n.advertise<sensor_msgs::JointState>("ur5/state", 10);

  // create the socket connection
  if((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
    ROS_ERROR("Could not create socket");
    exit(-1);
  }

  memset(&server, 0, sizeof(server));     /* Zero out structure */
  server.sin_family      = AF_INET;             /* Internet address family */
  server.sin_addr.s_addr = inet_addr(ur5IP.c_str());   /* Server IP address */
  server.sin_port        = htons(port); /* Server port */

  // connect

  if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
    ROS_ERROR("Could not connect to ur5");
    exit(-1);
  }
  
  // program the robot
  std::ifstream t("/home/rkeatin3/summer_ws/src/ME530646/ur5/jointSpace.urp", std::ifstream::in);
  std::stringstream buffer;
  buffer << t.rdbuf();
  t.close();
  program = buffer.str();

  // there must be an additional new line to start the program
  program += "/n";

  if(send(sock, program.c_str(), program.size(), 0) != program.size()) {
    ROS_ERROR("Could not program UR5");
    exit(-1);
  }

  // set up the joint state message
  // change joint names as necessary
  sensor_msgs::JointState ur5JointState;
  ur5JointState.name.push_back("base");
  ur5JointState.name.push_back("shoulder");
  ur5JointState.name.push_back("elbow");
  ur5JointState.name.push_back("wrist1");
  ur5JointState.name.push_back("wrist2");
  ur5JointState.name.push_back("wrist3");

  ur5JointState.position.resize(6);

  // run loop at 125Hz
  ros::Rate r(125);
  while(ros::ok()) {
    // receive some message
    receive(sock);

    // publish the message
    ur5JointState.position = getPositions();
    pub.publish(ur5JointState);
    r.sleep();
  }

  // close
  close(sock);
}






