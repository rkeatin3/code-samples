#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <dynamixel_msgs/JointState.h>
#include <dynamixel_controllers/SetTorqueLimit.h>

double load = 1;
double pos = 0;
double cmd = 0;

void stateCallback(const dynamixel_msgs::JointState state)
{
	load = state.load;
	pos = state.current_pos;
}

void commandCallback(const std_msgs::Float64 command)
{
	cmd = command.data;
}

int main(int argc, char** argv)
{
	ros::init(argc,argv,"hand_controller");
	ros::NodeHandle n;
	double hardlim = 0.40;	// Hard torque limit (ratio of limited torque to maxmimum torque) set by Dynamixel
    double softlim = 0.3;	// Soft torque limit set by this program
    double error = 0.2; 	// Error threshold for determining if hand has reached it's goal position
    double open = 1.0;	// Angle of dynamixel (radians) at which the hand is fully open
    double close = 5.0;	// Angle of dynamixel at which hand is fully closed

    //Creat publisher for command to tilt_controller
    ros::Publisher pb = n.advertise<std_msgs::Float64>("tilt_controller/command",10);
   	//Subscribe to receive information about motor state
    ros::Subscriber stateSB = n.subscribe("tilt_controller/state", 10, &stateCallback);
   	//Create call to call torque-limiting service
    ros::ServiceClient client = n.serviceClient<dynamixel_controllers::SetTorqueLimit>("SetTorqueLimit");
    //Subscribe to topic for open-close commands
    ros::Subscriber cmdSB = n.subscribe("hand_cmd", 10, &commandCallback);

    //Call torque-limiting service
    dynamixel_controllers::SetTorqueLimit srv;
    srv.request.torque_limit = hardlim;
    if (!client.call(srv))
    {
    	ROS_ERROR("Failed to call service to set dynamixel torque limit.");
    	return 1;
    }

    //Run loop at 1000Hz
    ros::Rate r(1000);
    std_msgs::Float64 toPub;
    while(ros::ok())
    {
    	//Publish open command
    	if(cmd == 0)
    	{
    		toPub.data = open;
    	}
    	else if(cmd == 1)
    	{
    		//Check that hand isn't already closed
    		while(fabs(pos-close) >= error)
    		{
    			//Check that soft torque limit not exceeded, then publish close command
    			if(fabs(load) < softlim)
    			{
    				toPub.data = close;
    			}
    			//Publish command to stay at current position
    			else
    			{
    				toPub.data = pos;
    				break;
    			}
    		}
    	}
    	//Error for non-sensical command
    	else
    	{
    		ROS_DEBUG("Invalid command to dynamixel motor.");
    		continue;
    	}
    	pb.publish(toPub);
    	r.sleep();
    }
}