#include <Eigen/Core>
#include <ur5/utilities.h>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <me530646_lab3/lab3.h>

#define PI M_PI

int main(int argc, char **argv){
	ros::init(argc,argv,"lab3");
	ros::NodeHandle n;
	UR5 robot = UR5(n);
	double q1[] = {1,5.2,2,3,4.5,0};
	robot.plotfwd(q1);
	robot.plotframes(q1);
	Eigen::Matrix4d T1 = getTransformation("base_link","ee_link");
	Eigen::Matrix4d T1A = UR5::fwd(q1);
	if(!matrixEquals(T1.block<3,1>(0,3),T1A.block<3,1>(0,3)))
		printf("Failed testing P06 for T1\n");
	if(!matrixEquals(T1.block<3,1>(0,0),T1A.block<3,1>(0,2)))
		printf("Failed testing x16 and z16 for T1\n");
	sleep(1);

	double q2[] = {0,3,2,4,3.4,2};
	robot.plotfwd(q2);
	robot.plotframes(q2);
	Eigen::Matrix4d T2 = getTransformation("base_link","ee_link");
	Eigen::Matrix4d T2A = UR5::fwd(q2);
	if(!matrixEquals(T2.block<3,1>(0,3),T2A.block<3,1>(0,3)))
		printf("Failed testing P06 for T2\n");
	if(!matrixEquals(T2.block<3,1>(0,0),T2A.block<3,1>(0,2)))
		printf("Failed testing x16 and z16 for T2\n");
	sleep(1);

	double q3[] = {3,.3,4.7,.7,3,0};
	robot.plotfwd(q3);
	robot.plotframes(q3);
	Eigen::Matrix4d T3 = getTransformation("base_link","ee_link");
	Eigen::Matrix4d T3A = UR5::fwd(q3);
	if(!matrixEquals(T3.block<3,1>(0,3),T3A.block<3,1>(0,3)))
		printf("Failed testing P06 for T3\n");
	if(!matrixEquals(T3.block<3,1>(0,0),T3A.block<3,1>(0,2)))
		printf("Failed testing x16 and z16 for T3\n");
	sleep(1);

	double q4[] = {1,6.1,2,0,2,.7};
	robot.plotfwd(q4);
	robot.plotframes(q4);
	Eigen::Matrix4d T4 = getTransformation("base_link","ee_link");
	Eigen::Matrix4d T4A = UR5::fwd(q4);
	if(!matrixEquals(T4.block<3,1>(0,3),T4A.block<3,1>(0,3)))
		printf("Failed testing P06 for T4\n");
	if(!matrixEquals(T4.block<3,1>(0,0),T4A.block<3,1>(0,2)))
		printf("Failed testing x16 and z16 for T4\n");

	double theta[5] = {-PI/3,5*PI/4,7*PI/8,PI/5,0};
	double a[5] = {3,2,6,1,0};
	double d[5] = {8,3,-1,5,2};
	double alpha[5] = {PI/2,PI,3*PI/4,-PI,PI/6};
	std::vector<Eigen::Matrix4d> dh;
	Eigen::Matrix4d dh0;
	dh0 << 0.5, 5.30288e-17, -0.866025, 1.5,  -0.866025, 3.06162e-17, -0.5, -2.59808,  0, 1, 6.12323e-17, 8,  0, 0, 0, 1;
	dh.push_back(dh0);
	Eigen::Matrix4d dh1;
	dh1 << -0.707107, -0.707107, -8.65956e-17, -1.41421,  -0.707107, 0.707107, 8.65956e-17, -1.41421,  0, 1.22465e-16, -1, 3,  0, 0, 0, 1;
	dh.push_back(dh1);
	Eigen::Matrix4d dh2;
	dh2 << -0.92388, 0.270598, 0.270598, -5.54328,  0.382683, 0.653281, 0.653281, 2.2961,  0, 0.707107, -0.707107, -1,  0, 0, 0, 1;
	dh.push_back(dh2);
	Eigen::Matrix4d dh3;
	dh3 << 0.809017, 0.587785, -7.19829e-17, 0.809017,  0.587785, -0.809017, 9.9076e-17, 0.587785,  0, -1.22465e-16, -1, 5,  0, 0, 0, 1;
	dh.push_back(dh3);
	Eigen::Matrix4d dh4;
	dh4 << 1, 0, 0, 0,  0, 0.866025, -0.5, 0,  0, 0.5, 0.866025, 2,  0, 0, 0, 1;
	dh.push_back(dh4);
	for(int i=0;i<5;i++)
	{
		if(!matrixEquals(UR5::dhf(alpha[i],a[i],d[i],theta[i]),dh.at(i)))
			printf("Failed testing for dhf at i = %d\n",i);
	}

}
