#include <Eigen/Core>
#include <ur5/utilities.h>
#include <unsupported/Eigen/MatrixFunctions>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <Eigen/Eigenvalues>

#define PI 3.1415926

int main(int argc, char **argv){
	//Initializing ROS
    ros::init(argc, argv, "lab1_main");
    ros::NodeHandle node;
    //Creating an RvizPlotter
	RvizPlotter p = RvizPlotter(node);
	
	Eigen::Vector3f v(2,1,-3);
	printf("Testing skew3");
	Eigen::Matrix3f vHat = skew3(v);
	printEigen(vHat);
	printf("Testing expr");
	Eigen::Matrix3f R = expr(v);
	printEigen(R);
	printf("Testing xfinv");
	Eigen::Matrix4f B2 = xf(2,1,2,PI/-.4,PI/4,PI/3);
	Eigen::VectorXf inv = xfinv(B2);
	printEigen(B2);
	printEigen(xf(inv(0),inv(1),inv(2),inv(3),inv(4),inv(5)));



	/*****Examples of how to use necessary functions*****/
	/*
	//Calculating the norm of vector v
	Eigen::Vector3f v = Eigen::MatrixXf::Random(3,1);
	double normV = v.norm();
	//Calculating the matrix expontential of matrix m
	Eigen::Matrix4f m = Eigen::MatrixXf::Random(4,4);
	Eigen::Matrix4f expM = m.exp();
	//Calculating the eigenvalues and eigenvectors of m
	Eigen::EigenSolver<Eigen::MatrixXf> es(m);
	//VectorXcf MatrixXcf are used because there may 
	//be complex values for eigenvalues and eigenvectors
	Eigen::VectorXcf evals = es.eigenvalues();
	Eigen::MatrixXcf evecs = es.eigenvectors();
	*/
	/****************************************************/

	

}