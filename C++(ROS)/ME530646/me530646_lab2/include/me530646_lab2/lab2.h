#ifndef LAB2_H
#define LAB2_H

#include <Eigen/Dense>

/**
 * Returns the 3x3 skew-symmetric matrix that corresponds to vector e.
 */
Eigen::Matrix3d skew3(Eigen::Vector3d e);

/**
 * Returns the 3x3 rotation matrix that corresponds to a rotation of 
 * |e| radians about vector e.
 */
Eigen::Matrix3d expr(Eigen::Vector3d e);

/**
 * Returns the (x,y,z) translation and (roll,pitch,yaw) rotations
 * of the given homogeneous transformation.
 */
Eigen::VectorXd xfinv(Eigen::Matrix4d xf);

#endif