#include <Eigen/Core>
#include <ur5/utilities.h>
#include <unsupported/Eigen/MatrixFunctions>
#include <me530646_lab1/lab1.h>
#include <me530646_lab2/lab2.h>
#include <Eigen/Eigenvalues>

#define PI M_PI

int main(int argc, char **argv){
	//Initializing ROS
    ros::init(argc, argv, "lab1_main");
    ros::NodeHandle node;
    //Creating an RvizPlotter
	RvizPlotter p = RvizPlotter(node);
	
	/*****Examples of how to use necessary functions*****/
	//Calculating the norm of vector v
	Eigen::Vector3f v = Eigen::MatrixXf::Random(3,1);
	double normV = v.norm();
	//Calculating the matrix expontential of matrix m
	Eigen::Matrix4d m = Eigen::MatrixXd::Random(4,4);
	Eigen::Matrix4d expM = m.exp();
	//Calculating the eigenvalues and eigenvectors of m
	Eigen::EigenSolver<Eigen::MatrixXd> es(m);
	//VectorXcf MatrixXcf are used because there may 
	//be complex values for eigenvalues and eigenvectors
	Eigen::VectorXcd evals = es.eigenvalues();
	Eigen::MatrixXcd evecs = es.eigenvectors();
	/****************************************************/

	

}