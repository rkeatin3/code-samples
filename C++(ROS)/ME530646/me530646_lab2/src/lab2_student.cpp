#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>
#include <me530646_lab1/lab1.h>
#include <Eigen/Eigenvalues>

/**
 * Returns the 3x3 skew-symmetric matrix that corresponds to vector e.
 */
Eigen::Matrix3d skew3(Eigen::Vector3f e){
	//TODO
}

/**
 * Returns the 3x3 rotation matrix that corresponds to a rotation of 
 * |e| radians about vector e.
 */
Eigen::Matrix3d expr(Eigen::Vector3d e){
	//TODO
}

/**
 * Returns the (x,y,z) translation and (roll,pitch,yaw) rotations
 * of the given homogeneous transformation.
 */
Eigen::VectorXd xfinv(Eigen::Matrix4d xf){
	Eigen::VectorXd xyzrpy(6);
	//TODO
	return xyzrpy;
}


