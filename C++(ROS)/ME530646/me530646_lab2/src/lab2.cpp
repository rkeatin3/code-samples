#include <Eigen/Dense>
#include <ur5/utilities.h>
#include <unsupported/Eigen/MatrixFunctions>
#include <me530646_lab1/lab1.h>

/**
 * Returns the 3x3 skew-symmetric matrix that corresponds to vector e.
 */
Eigen::Matrix3d skew3(Eigen::Vector3d e){
	Eigen::Matrix3d j;
	j <<     0, -e(2),  e(1),
		  e(2),     0, -e(0),
		 -e(1),  e(0),     0;

	return j;
}

/**
 * Returns the 3x3 rotation matrix that corresponds to a rotation of 
 * |e| radians about vector e.
 */
Eigen::Matrix3d expr(Eigen::Vector3d e){
	Eigen::Matrix3d r = skew3(e).exp();
	
	return r;
}

/**
 * Returns the (x,y,z) translation and (roll,pitch,yaw) rotations
 * of the given homogeneous transformation.
 */
Eigen::VectorXd xfinv(Eigen::Matrix4d xf){
	Eigen::VectorXd xyzrpy(6);
	xyzrpy(0) = xf(0,3);
	xyzrpy(1) = xf(1,3);
	xyzrpy(2) = xf(2,3);

	Eigen::MatrixXd r = xf.block<3,3>(0,0);
	Eigen::Vector3d rpy = rpyrinv(r);
	xyzrpy(3) = rpy(0);
	xyzrpy(4) = rpy(1);
	xyzrpy(5) = rpy(2);

	return xyzrpy;
}